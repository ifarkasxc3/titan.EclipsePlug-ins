/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST;

import java.io.Reader;
import java.io.StringReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.SWT;
import org.eclipse.titan.designer.GeneralConstants;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.parsers.ttcn3parser.Ttcn3DocCommentLexer;
import org.eclipse.titan.designer.parsers.ttcn3parser.Ttcn3DocCommentParser;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;

/**
 * A helper class for documentation comment handling
 * 
 * ETSI ES 201 873-10 V4.5.1 (2013-04)
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class DocumentComment implements ILocateableNode {
	private static final String IDPATTERN = "^[A-Za-z][A-Za-z0-9_]*$";

	/* Raw text of the comment */
	private String comment;

	/* First and last tokens of the comment */
	private Token first;
	private Token last;

	/* Location of the document comment. 
	 * 
	 * For now, all semantic problems will be reported for the whole comment. 
	 **/
	private Location location;

	private final List<String> authors = new ArrayList<String>();
	private String config = null;
	private final List<String> descs = new ArrayList<String>();
	private final Map<String,String> exceptions = new HashMap<String, String>();
	private final Map<String,String> members = new HashMap<String, String>();
	private final Map<String,String> params = new HashMap<String, String>();
	private String priority = null;
	private String purpose = null;
	private String reference = null;
	private final List<String> remarks = new ArrayList<String>();
	private final List<String> requirements = new ArrayList<String>();
	private String return_ = null;
	private final List<String> sees = new ArrayList<String>();
	private String since = null;
	private String status = null;
	private final List<String> urls = new ArrayList<String>();
	private final Map<String,String> verdicts = new HashMap<String, String>();
	private String version = null;

	public DocumentComment(final String comment, final Token first, final Token last) {
		this.comment = comment;
		this.first = first;
		this.last = last;
	}

	public Token getFirstToken() {
		return first;
	}

	public Token getLastToken() {
		return last;
	}

	@Override
	public void setLocation(final Location location) {
		this.location = location; 
	}

	@Override
	public Location getLocation() {
		return location;
	}

	/**
	 * Adds author(s)
	 * @param text Author(s)
	 */
	public void addAuthor(final String text) {
		if (text != null) {
			authors.add(text.trim());
		}
	}

	/**
	 * Adds authors' content to hover info
	 * @param content Content of hover info
	 */
	public void addAuthorsContent(Ttcn3HoverContent content) {
		if (authors.size() > 0) {
			content.addTag("Authors:");
			for (String author : authors) {
				content.addIndentedText(author, SWT.ITALIC);
			}
		}
	}

	/**
	 * Returns the list of authors
	 * @return List of authors
	 */
	public List<String> getAuthors() {
		return authors;
	}

	/**
	 * Returns whether {@code @author} tag is specified
	 * @return {@code true} if {@code @author} tag is specified, {@code false} otherwise
	 */
	public boolean hasAuthors() {
		return authors.size() > 0;
	}

	/**
	 * Adds the test configuration's description 
	 * @param text Description of the test configuration
	 */
	public void addConfig(final String text) {
		if (config == null) {
			if (text != null) {
				config = text.trim();
			}
		} else {
			reportProblem("`@config' tag is specified more than once");
		}
	}

	/**
	 * Adds the configuration's content to hover info
	 * @param content Content of hover info
	 */
	public void addConfigContent(Ttcn3HoverContent content) {
		if (config == null) {
			return;
		}
		content.addTag("Configuration:");
		content.addIndentedText(config);
	}

	/**
	 * Returns the configuration's description. If it is not specified, empty string is returned.
	 * @return Description of the configuration
	 */
	public String getConfig() {
		return config != null ? config : "";
	}

	/**
	 * Returns whether {@code @config} tag is specified
	 * @return {@code true} if {@code @config} tag is specified, {@code false} otherwise
	 */
	public boolean hasConfig() {
		return config != null;
	}

	/**
	 * Adds description(s)
	 * @param text Description(s)
	 */
	public void addDesc(final String text) {
		if (text != null) {
			descs.add(text.trim());
		}
	}

	/**
	 * Adds descriptions' content to hover info
	 * @param content Content of hover info
	 */
	public void addDescsContent(Ttcn3HoverContent content) {
		if (descs.size() > 0) {
			content.addText("<br>");
			for (String desc : descs) {
				content.addText(desc).addText(" ");
			}
		}
	}

	/**
	 * Returns the list of descriptions
	 * @return List of descriptions
	 */
	public List<String> getDescs() {
		return descs;
	}

	/**
	 * Returns whether {@code @desc} tag is specified
	 * @return {@code true} if {@code @desc} tag is specified, {@code false} otherwise
	 */
	public boolean hasDescs() {
		return descs.size() > 0;
	}

	/**
	 * Adds description to the specified exception. The text can be {@code null}.
	 * @param id Exception to describe
	 * @param text Description of the exception
	 */
	public void addException(final String id, final String text) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		final String prev = exceptions.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this exception is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format("The exception `{0}'' is specified more than once, using the latest", trimmedId));
		}
	}

	/**
	 * Adds exceptions' content to hover info
	 * @param content Content of hover info
	 */
	public void addExceptionsContent(Ttcn3HoverContent content) {
		if (exceptions.size() > 0) {
			content.addTag("Exceptions");
			for (Map.Entry<String, String> exception : exceptions.entrySet()) {
				content.addIndentedText(exception.getKey(), exception.getValue());
			}
		}
	}

	/**
	 * Returns the map of exceptions' descriptions
	 * @return Map of exceptions' descriptions
	 */
	public Map<String, String> getExceptions() {
		return exceptions;
	}

	/**
	 * Returns whether {@code @exception} tag is specified
	 * @return {@code true} if {@code @exception} tag is specified, {@code false} otherwise
	 */
	public boolean hasExceptions() {
		return exceptions.size() > 0;
	}

	/**
	 * Adds description to the specified member. The text can be {@code null}.
	 * @param id Member to describe
	 * @param text Description of the member
	 */
	public void addMember(final String id, final String text) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		final String prev = members.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this member is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format("The member `{0}'' is specified more than once, using the latest", trimmedId));
		}
	}

	/**
	 * Adds members' content to hover info
	 * @param content Content of hover info
	 */
	public void addMembersContent(Ttcn3HoverContent content) {
		if (members.size() > 0) {
			content.addTag("Members");
			for (Map.Entry<String, String> member : members.entrySet()) {
				content.addIndentedText(member.getKey(), member.getValue());
			}
		}
	}

	/**
	 * Returns the map of object members
	 * @return Map of object members
	 */
	public Map<String, String> getMembers() {
		return members;
	}

	/**
	 * Returns whether {@code @member} tag is specified
	 * @return {@code true} if {@code @member} tag is specified, {@code false} otherwise
	 */
	public boolean hasMembers() {
		return members.size() > 0;
	}

	/**
	 * Adds description to the specified parameter. The text can be {@code null}.
	 * @param id Parameter to describe
	 * @param text Description of the parameter
	 */
	public void addParam(final String id, final String text) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim();
		final String prev = params.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this parameter is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format("The parameter `{0}'' is specified more than once, using the latest", trimmedId));
		}
	}

	/**
	 * Adds parameters' content to hover info
	 * @param content Content of hover info
	 */
	public void addParamsContent(Ttcn3HoverContent content, final FormalParameterList fpl) {
		if (params.size() > 0) {
			content.addTag("Parameters");
			String paramType = null;
			for (Map.Entry<String, String> param : params.entrySet()) {
				FormalParameter fp = null;
				if (fpl != null) {
					fp = fpl.getParameterById(new Identifier(Identifier_type.ID_TTCN, param.getKey()));
				}
				if (fp != null) {
					paramType = fp.getFormalParamType();
				} else {
					reportProblem(MessageFormat.format(
							"The formal parameter `{0}'' in the comment is not part of the definition", param.getKey()));
				}
				final StringBuilder sb = new StringBuilder(param.getKey());
				sb.append(" (").append(paramType != null ? paramType : "<?>").append(")");
				content.addIndentedText(sb.toString(), param.getValue());
			}
		}
	}

	/**
	 * Returns the map of parameters
	 * @return Map of parameters
	 */
	public Map<String, String> getParams() {
		return params;
	}

	/**
	 * Returns whether {@code @param} tag is specified
	 * @return {@code true} if {@code @param} tag is specified, {@code false} otherwise
	 */
	public boolean hasParams() {
		return params.size() > 0;
	}

	/**
	 * Adds the priority category of the test case
	 * @param text Priority category of the test case
	 */
	public void addPriority(final String text) {
		if (priority == null) {
			if (text != null) {
				priority = text.trim();
			}
		} else {
			reportProblem("`@priority' tag is specified more than once");
		}
	}

	/**
	 * Adds the priority content to hover info
	 * @param content Content of hover info
	 */
	public void addPriorityContent(Ttcn3HoverContent content) {
		if (priority == null) {
			return;
		}
		content.addTagWithText("Priority:", priority);
	}

	/**
	 * Returns the priority category of the test case. If it is not specified, empty string is returned.
	 * @return Priority category of the test case
	 */
	public String getPriority() {
		return priority != null ? priority : "";
	}

	/**
	 * Returns whether {@code @priority} tag is specified
	 * @return {@code true} if {@code @priority} tag is specified, {@code false} otherwise
	 */
	public boolean hasPriority() {
		return priority != null;
	}

	/**
	 * Adds description to the {@code @purpose} tag
	 * @param text Description of the {@code @purpose} tag
	 */
	public void addPurpose(final String text) {
		if (purpose == null) {
			if (text != null) {
				purpose = text.trim();
			}
		} else {
			reportProblem("`@purpose' tag is specified more than once");
		}
	}

	/**
	 * Adds the purpose content to hover info
	 * @param content Content of hover info
	 */
	public void addPurposeContent(Ttcn3HoverContent content) {
		if (purpose == null) {
			return;
		}
		content.addTagWithText("Purpose:", purpose);
	}

	/**
	 * Returns the purpose. If it is not specified, empty string is returned.
	 * @return Purpose
	 */
	public String getPurpose() {
		return purpose != null ? purpose : "";
	}

	/**
	 * Returns whether {@code @purpose} tag is specified
	 * @return {@code true} if {@code @purpose} tag is specified, {@code false} otherwise
	 */
	public boolean hasPurpose() {
		return purpose != null;
	}

	/**
	 * Adds reference
	 * @param text Reference
	 */
	public void addReference(final String text) {
		if (reference == null) {
			if (text != null) {
				reference = text.trim();
			}
		} else {
			reportProblem("`@reference' tag is specified more than once");
		}
	}

	/**
	 * Adds the reference content to hover info
	 * @param content Content of hover info
	 */
	public void addReferenceContent(Ttcn3HoverContent content) {
		if (reference == null) {
			return;
		}
		content.addTagWithText("Reference:", reference);
	}

	/**
	 * Returns the reference. If it is not specified, empty string is returned.
	 * @return Reference
	 */
	public String getReference() {
		return reference != null ? reference : "";
	}

	/**
	 * Returns whether {@code @reference} tag is specified
	 * @return {@code true} if {@code @reference} tag is specified, {@code false} otherwise
	 */
	public boolean hasReference() {
		return reference != null;
	}

	/**
	 * Adds description to the {@code @remark} tag
	 * @param text Description of the {@code @remark} tag
	 */
	public void addRemark(final String text) {
		if (text != null) {
			remarks.add(text.trim());
		}
	}

	/**
	 * Adds remarks' content to hover info
	 * @param content Content of hover info
	 */
	public void addRemarksContent(Ttcn3HoverContent content) {
		if (remarks.size() > 0) {
			content.addTag("Remarks");
			for (String remark : remarks) {
				content.addText(remark);
			}
		}
	}

	/**
	 * Returns the list of remarks
	 * @return List of remarks
	 */
	public List<String> getRemarks() {
		return remarks;
	}

	/**
	 * Returns whether {@code @remark} tag is specified
	 * @return {@code true} if {@code @remark} tag is specified, {@code false} otherwise
	 */
	public boolean hasRemarks() {
		return remarks.size() > 0;
	}

	/**
	 * Adds requirement to the test case
	 * @param text Requirement of the test case
	 */
	public void addRequirement(final String text) {
		if (text != null) {
			requirements.add(text.trim());
		}
	}

	/**
	 * Adds requirements' content to hover info
	 * @param content Content of hover info
	 */
	public void addRequirementsContent(Ttcn3HoverContent content) {
		if (requirements.size() > 0) {
			content.addTag("Requirements");
			for (String requirement : requirements) {
				content.addIndentedText(requirement);
			}
		}
	}

	/**
	 * Returns the list of requirements
	 * @return List of requirements
	 */
	public List<String> getRequirements() {
		return requirements;
	}

	/**
	 * Returns whether {@code @requirement} tag is specified
	 * @return {@code true} if {@code @requirement} tag is specified, {@code false} otherwise
	 */
	public boolean hasRequirements() {
		return requirements.size() > 0;
	}

	/**
	 * Adds description to the return statement
	 * @param text Description of the return statement
	 */
	public void addReturn(final String text) {
		if (return_ == null) {
			if (text != null) {
				return_ = text.trim();
			}
		} else {
			reportProblem("`@return' tag is specified more than once");
		}
	}

	/**
	 * Adds return statement's content to hover info
	 * @param content Content of hover info
	 */
	public void addReturnContent(Ttcn3HoverContent content) {
		if (return_ == null) {
			return;
		}
		content.addTag("Returns");
		content.addIndentedText(return_);
	}

	/**
	 * Returns the returns statement's description. If it is not specified, empty string is returned.
	 * @return Description of the return statement
	 */
	public String getReturn() {
		return return_ != null ? return_ : "";
	}

	/**
	 * Returns whether {@code @return} tag is specified
	 * @return {@code true} if {@code @return} tag is specified, {@code false} otherwise
	 */
	public boolean hasReturn() {
		return return_ != null;
	}

	/**
	 * Adds the identifier of {@code @see} tag
	 * @param text Identifier
	 */
	public void addSee(final String text) {
		if (text != null) {
			sees.add(text.trim());
		}
	}

	/**
	 * Adds content of {@code @see} tags' identifiers to hover info
	 * @param content Content of hover info
	 */
	public void addSeesContent(Ttcn3HoverContent content) {
		if (sees.size() > 0) {
			content.addTag("See:");
			for (String see : sees) {
				content.addIndentedText(see, SWT.ITALIC);
			}
		}
	}

	/**
	 * Returns the list of {@code @see} tags' identifiers
	 * @return List of {@code @see} tags' identifiers
	 */
	public List<String> getSees() {
		return sees;
	}

	/**
	 * Returns whether {@code @see} tag is specified
	 * @return {@code true} if {@code @see} tag is specified, {@code false} otherwise
	 */
	public boolean hasSees() {
		return sees.size() > 0;
	}

	/**
	 * Adds description to the {@code @since} tag
	 * @param text Description of the {@code @since} tag
	 */
	public void addSince(final String text) {
		if (since == null) {
			if (text != null) {
				since = text.trim();
			}
		} else {
			reportProblem("`@since' tag is specified more than once");
		}
	}

	/**
	 * Adds {@code @since} tag's content to hover info
	 * @param content Content of hover info
	 */
	public void addSinceContent(Ttcn3HoverContent content) {
		if (since == null) {
			return;
		}
		content.addTagWithText("Since:", since);
	}

	/**
	 * Returns {@code @since} tag's description. If it is not specified, empty string is returned.
	 * @return Description of the {@code @since} tag
	 */
	public String getSince() {
		return since != null ? since : "";
	}

	/**
	 * Returns whether {@code @since} tag is specified
	 * @return {@code true} if {@code @since} tag is specified, {@code false} otherwise
	 */
	public boolean hasSince() {
		return since != null;
	}

	/**
	 * Adds description to the {@code @status} tag
	 * @param text Description of the {@code @status} tag
	 */
	public void addStatus(final String text) {
		if (status == null) {
			if (text != null) {
				status = text.trim();
			}
		} else {
			reportProblem("`@status' tag is specified more than once");
		}
	}

	/**
	 * Adds {@code @status} tag's content to hover info
	 * @param content Content of hover info
	 */
	public void addStatusContent(Ttcn3HoverContent content) {
		if (status == null) {
			return;
		}
		content.addTagWithText("Status:", status);
	}

	/**
	 * Returns {@code @status} tag's description. If it is not specified, empty string is returned.
	 * @return Description of the {@code @status} tag
	 */
	public String getStatus() {
		return status != null ? status : "";
	}

	/**
	 * Returns whether {@code @status} tag is specified
	 * @return {@code true} if {@code @status} tag is specified, {@code false} otherwise
	 */
	public boolean hasStatus() {
		return status != null;
	}

	/**
	 * Adds the text of {@code @url} tag
	 * @param text URL
	 */
	public void addUrl(final String text) {
		if (text != null) {
			urls.add(text.trim());
		}
	}

	/**
	 * Adds content of URLs to hover info
	 * @param content Content of hover info
	 */
	public void addUrlsContent(Ttcn3HoverContent content) {
		if (urls.size() > 0) {
			content.addTag("URL:");
			for (String url : urls) {
				content.addUrlAsLink(url, SWT.ITALIC);
			}
		}
	}

	/**
	 * Returns the list of URLs
	 * @return List of URLs
	 */
	public List<String> getUrls() {
		return urls;
	}

	/**
	 * Returns whether {@code @url} tag is specified
	 * @return {@code true} if {@code @url} tag is specified, {@code false} otherwise
	 */
	public boolean hasUrls() {
		return urls.size() > 0;
	}

	/**
	 * Adds description to the specified verdict type. The text can be {@code null}.
	 * @param id Verdict type to describe
	 * @param text Description of the verdict type
	 */
	public void addVerdict(final String id, final String text) {
		if (id == null) {
			return;
		}
		final String trimmedId = id.trim().toLowerCase();
		// Check the verdict type for 'pass', 'fail', 'inconc'
		if (!trimmedId.equals("pass") && !trimmedId.equals("fail") && !trimmedId.equals("inconc")) {
			reportProblem(MessageFormat.format("The verdict `{0}'' is not allowed.\n"
					+ "Allowed verdicts are: `pass', `fail', `inconc'", trimmedId));
			return;
		}
		final String prev = verdicts.put(trimmedId, text != null ? text.trim() : "");
		// This key is already present in the map, i.e. this exception is already specified in the comment
		if (prev != null) {
			reportProblem(MessageFormat.format("The verdict `{0}'' is specified more than once, using the latest", trimmedId));
		}
	}

	/**
	 * Adds verdicts' content to hover info
	 * @param content Content of hover info
	 */
	public void addVerdictsContent(Ttcn3HoverContent content) {
		if (verdicts.size() > 0) {
			content.addTag("Verdicts");
			for (Map.Entry<String, String> verdict : verdicts.entrySet()) {
				content.addIndentedText(verdict.getKey(), verdict.getValue());
			}
		}
	}

	/**
	 * Returns the map of verdicts' descriptions
	 * @return Map of verdicts' descriptions
	 */
	public Map<String, String> getVerdicts() {
		return verdicts;
	}

	/**
	 * Returns whether {@code @verdict} tag is specified
	 * @return {@code true} if {@code @verdict} tag is specified, {@code false} otherwise
	 */
	public boolean hasVerdicts() {
		return verdicts.size() > 0;
	}

	/**
	 * Adds the version
	 * @param text Version
	 */
	public void addVersion(final String text) {
		if (version == null) {
			if (text != null) {
				version = text.trim();
			}
		} else {
			reportProblem("`@version' tag is specified more than once");
		}
	}

	/**
	 * Adds the version's content to hover info
	 * @param content Content of hover info
	 */
	public void addVersionContent(Ttcn3HoverContent content) {
		if (version == null) {
			return;
		}
		content.addTagWithText("Version:", version);
	}

	/**
	 * Returns the version. If it is not specified, empty string is returned.
	 * @return Version
	 */
	public String getVersion() {
		return version != null ? version : "";
	}

	/**
	 * Returns whether {@code @version} tag is specified
	 * @return {@code true} if {@code @version} tag is specified, {@code false} otherwise
	 */
	public boolean hasVersion() {
		return version != null;
	}

	/**
	 * Clears the comments and descriptions related to the different tags
	 */
	public void clear() {
		authors.clear();
		config = null;
		descs.clear();
		exceptions.clear();
		members.clear();
		params.clear();
		priority = null;
		purpose = null;
		reference = null;
		remarks.clear();
		requirements.clear();
		return_ = null;
		sees.clear();
		since = null;
		status = null;
		urls.clear();
		verdicts.clear();
		version = null;
	}

	/**
	 * Parses the comment section
	 */
	public void parseComment() {
		clear();
		final Reader reader = new StringReader(comment);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3DocCommentLexer lexer = new Ttcn3DocCommentLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		
		// Pls. do not remove this comment, the below line is kept for later debugging
		//List<? extends Token> tokens = lexer.getAllTokens();
		
		final CommonTokenStream cts = new CommonTokenStream(lexer);
 		final Ttcn3DocCommentParser parser = new Ttcn3DocCommentParser(cts);
		parser.pr_DocComment(this);
	}

	/**
	 * Helper function for reporting semantic errors/warning according to the related preference 
	 * @param reason the reason of the problem
	 * @see PreferenceConstants#REPORT_DOC_COMMENT_INCONSISTENCY
	 */
	private void reportProblem(final String reason) {
		final String option = Platform.getPreferencesService().getString(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.REPORT_DOC_COMMENT_INCONSISTENCY, GeneralConstants.WARNING, null);
		getLocation().reportConfigurableSemanticProblem(option, reason);
	}
}
