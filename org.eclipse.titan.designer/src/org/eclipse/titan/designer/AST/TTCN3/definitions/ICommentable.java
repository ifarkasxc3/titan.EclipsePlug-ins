/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.ui.IEditorPart;

/**
 * This interface represents the language element that can have document comment
 * (information, source code, etc.)
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public interface ICommentable {
	/**
	 * Returns the object containing the documentation comment 
	 * @return the object containing the documentation comment
	 */
	public DocumentComment getDocumentComment();

	/**
	 * Returns whether the documentation comment exists for this node 
	 * @return {@code true} if the documentation comment is available, {@code false} otherwise
	 */
	public boolean hasDocumentComment();

	/**
	 * Parses the documentation comment and returns the documentation comment object containing the comments by tags
	 * @return the documentation comment object containing the comments by tags 
	 */
	public DocumentComment parseDocumentComment();

	/**
	 * Sets the documentation comment for this definition (ETSI ES 201 873-10 format)
	 * @param docComment the unparsed documentation comment object
	 */
	public void setDocumentComment(DocumentComment docComment);

	/** 
	 * Gets information for the given node that could be displayed in a source editor hover
	 * @param editor 
	 * @return
	 */
	Ttcn3HoverContent getHoverContent(IEditorPart editor);
}
