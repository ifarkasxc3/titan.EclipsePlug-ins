/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.AST.FieldSubReference;
import org.eclipse.titan.designer.AST.ILocateableNode;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IReferenceChainElement;
import org.eclipse.titan.designer.AST.ISubReference;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.TTCN3.IIncrementallyUpdateable;
import org.eclipse.titan.designer.AST.TTCN3.TTCN3Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Timer;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.VisibilityModifier;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the body of a class type.
 *
 * @author Miklos Magyari
 * */
public final class ClassTypeBody extends TTCN3Scope implements IReferenceChainElement, ILocateableNode, IIncrementallyUpdateable {
	public static final String MEMBERNOTVISIBLE = "Member `{0}'' in class type `{1}'' is not visible in this scope";
	public static final String LOCALINHERITANCECOLLISION = "Local Definiton `{0}'' collides with definition inherited from class type `{1}''";
	public static final String INHERITEDLOCATION = "Inherited definition of `{0}'' is here";
	public static final String INHERITANCECOLLISION =
			"Definition `{0}'' inherited from class type `{1}'' collides with definition inherited from `{2}''";
	public static final String INHERITEDDEFINITIONLOCATION = "Definition `{0}'' inherited from class type `{1}'' is here";
	public static final String HIDINGSCOPEELEMENT = "The name of the inherited definition `{0}'' is not unique in the scope hierarchy";
	public static final String HIDDENSCOPEELEMENT = "Previous definition with identifier `{0}'' in higher scope unit is here";
	public static final String HIDINGMODULEIDENTIFIER = "Inherited definition with name `{0}'' hides a module identifier";
	public static final String FORMALPARAMSDIFFER = "Formal parameter list differs from previous definition";
	public static final String OVERRIDDENFORMALPARAM = "Definition is overridden with different formal parameters";
	public static final String PUBLICOVERRIDEPUBLIC = "Public method can only be overriden by a public method";
	public static final String PROTECTEDOVERRIDE = "Protected method can only be overriden by a public or protected method";
	public static final String RETURNTYPEMISMATCH = "Return type differs from the overridden method's return type";
	public static final String OVERRIDDENRETURNTYPE = "Method is overridden with a different return type";
	public static final String VISIBILITYPRIVATEPROTECTED = "Class field visibility must be private or protected";
	public static final String TRAITMETHODSONLY = "Trait classes can only declare methods";
	public static final String TRAITMETHODHASBODY = "Trait method cannot have a function body";
	public static final String TRAITCONSTRUCTOR = "Trait classes cannot have a constructor";
	public static final String TRAITMETHODABSTRACT = "Trait classes can only declare abstract methods";
	public static final String ABSTRACTMETHODHASBODY = "Abstract method cannot have a function body";
	public static final String FIELDNOOVERRIDE = "A field of any visibility cannot be overriden by a subclass";
	
	private final Identifier identifier;
	/** The class's own definitions */
	private final DefinitionContainer definitions = new DefinitionContainer();
	
	/** class references from the extends part or null if none */
	private final ClassTypeReferenceList extendsReferences;
	
	/**
	 * The list of definitions gained through extends references.
	 **/
	private final Map<String, Definition> extendsGainedDefinitions = new HashMap<String, Definition>();

	private final Set<ClassTypeBody> compatibleBodies = new HashSet<ClassTypeBody>();
	
	private Class_Type myType;
	private Location location;
	private boolean hasExplicitContructor = false;
	
	private CompilationTimeStamp lastUniquenessCheck;
	private CompilationTimeStamp lastCompilationTimeStamp;
	
	public ClassTypeBody(final Identifier identifier, final ClassTypeReferenceList extendsReferences) {
 		this.identifier = identifier;
		this.extendsReferences = extendsReferences;
	
		if (extendsReferences != null) {
			extendsReferences.setFullNameParent(this);
		}
	}
	
	/**
	 * Adds assignments (right now only definitions) the this class type
	 * body.
	 *
	 * @param assignments the assignments to be added.
	 * */
	public void addAssignments(final List<Definition> assignments) {
		for (final Definition def : assignments) {
			if (def != null && def.getIdentifier() != null && def.getIdentifier().getLocation() != null) {
				addDefinition(def);
				if (isConstructor(def)) {
					hasExplicitContructor = true;
				}
			}
		}
	}
	
	public void addDefinition(Definition def) {
		def.setInherited(false);
		definitions.add(def);
		def.setFullNameParent(this);
		def.setMyScope(this);		
	}
	
	public void setMyType(final Class_Type type) {
		myType = type;
	}
	
	public Class_Type getMyType() {
		return myType;
	}
	
	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}
	
	public void setMyScope(final Scope scope) {
		setParentScope(scope);
		if (location != null && scope != null) {
			scope.addSubScope(location, this);
		}

		if (extendsReferences != null) {
			extendsReferences.setMyScope(scope);
		}
	}

	public List<Definition> getDefinitions() {
		final List<Definition> defs = definitions.getDefinitions();

		if (extendsGainedDefinitions.size() > 0) {
			for (Map.Entry<String, Definition> def : extendsGainedDefinitions.entrySet()) {
				if (!defs.contains(def.getValue())) {
					defs.add(def.getValue());
				}
			}
		}
		return defs;
	}
	
	public List<Definition> getOwnDefinitions() {
		return definitions.getDefinitions();
	}
	
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference) {
		return getAssBySRef(timestamp, reference, null);
	}

	@Override
	/** {@inheritDoc} */
	public Assignment getAssBySRef(final CompilationTimeStamp timestamp, final Reference reference, final IReferenceChain refChain) {
		if (reference.getModuleIdentifier() != null) {
			return getParentScope().getAssBySRef(timestamp, reference);
		}
		
		if (lastUniquenessCheck == null) {
			checkUniqueness(timestamp);
		}
		
		final String name = reference.getId().getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return definition;
		}

		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			if (VisibilityModifier.Public.equals(definition.getVisibilityModifier()) ||
				VisibilityModifier.Protected.equals(definition.getVisibilityModifier())) {
				return definition;
			}

			reference.getLocation().reportSemanticError(MessageFormat.format(
					MEMBERNOTVISIBLE, reference.getId().getDisplayName(), identifier.getDisplayName()));
			return null;
		}

		return getParentScope().getAssBySRef(timestamp, reference);
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean hasAssignmentWithId(final CompilationTimeStamp timestamp, final Identifier identifier) {
		final String name = identifier.getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return true;
		}

		return super.hasAssignmentWithId(timestamp, identifier);
	}
	
	public Assignment getAssByIdentifier(final CompilationTimeStamp timestamp, final Identifier identifier) {
		final String name = identifier.getName();
		Definition definition = definitions.getDefinition(name);
		if (definition != null) {
			return definition;
		}
		definition = extendsGainedDefinitions.get(name);
		if (definition != null) {
			return definition;
		}
		return null;
	}
	
	/**
	 * Checks the uniqueness of the definitions, and also builds a hashmap of
	 * them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (lastUniquenessCheck != null && !lastUniquenessCheck.isLess(timestamp)) {
			return;
		}

		lastUniquenessCheck = timestamp;

		compatibleBodies.clear();
		definitions.checkUniqueness();

		addDefinitionsOfExtendsParents(timestamp);
		// addDefinitionsOfExtendAttributeParents(timestamp);
	}
	
	private void addDefinitionsOfExtendsParents(final CompilationTimeStamp timestamp) {
		extendsGainedDefinitions.clear();
		
		if (extendsReferences == null) {
			return;
		}
		
		extendsReferences.check(timestamp);
		
		final List<ClassTypeBody> bodies = getExtendsInheritedClassBodies(timestamp);
		for (final ClassTypeBody body : bodies) {
			final Map<String, Definition> subDefinitionMap = body.getDefinitionMap();
			for (final Definition definition : subDefinitionMap.values()) {
				final String name = definition.getIdentifier().getName();
				if (definitions.hasDefinition(name)) {
					final Definition localDefinition = definitions.getDefinition(name);
					if (localDefinition.isInherited()) {
						continue;
					}
					final VisibilityModifier modifier = definition.getVisibilityModifier();
					if (!(definition instanceof Def_Function)) {
						localDefinition.getLocation().reportSemanticError(FIELDNOOVERRIDE);
						continue;
					}
					if (modifier == VisibilityModifier.Public || modifier == VisibilityModifier.Protected) {
						if (definition instanceof Def_Function && localDefinition instanceof Def_Function ) {
							if (! isConstructor(definition)) {
								final IType returnType = definition.getType(timestamp);
								final IType localReturnType = localDefinition.getType(timestamp);
								final Type_type type = returnType != null ? returnType.getTypetypeTtcn3() : null;
								final Type_type localType = localReturnType != null ? localReturnType.getTypetypeTtcn3() : null;
								if (type != localType) {
									if (localReturnType != null) {
										localReturnType.getLocation().reportSemanticError(RETURNTYPEMISMATCH);
									}
									definition.getType(timestamp).getLocation().reportSemanticError(OVERRIDDENRETURNTYPE);
								}
								if (! ((Def_Function)definition).getFormalParameterList().isSame(timestamp, ((Def_Function)localDefinition).getFormalParameterList())) {
									localDefinition.getIdentifier().getLocation().reportSemanticError(FORMALPARAMSDIFFER);
									definition.getIdentifier().getLocation().reportSemanticError(OVERRIDDENFORMALPARAM);
								}
								if (modifier == VisibilityModifier.Public && localDefinition.getVisibilityModifier() != VisibilityModifier.Public) {
									localDefinition.getIdentifier().getLocation().reportSemanticError(PUBLICOVERRIDEPUBLIC);
								}
								if (modifier == VisibilityModifier.Protected) {
									final VisibilityModifier mod = localDefinition.getVisibilityModifier();
									if (mod != VisibilityModifier.Protected && mod != VisibilityModifier.Public) {
										localDefinition.getIdentifier().getLocation().reportSemanticError(PROTECTEDOVERRIDE);
									}
								}
							}
						} else {
							localDefinition.getIdentifier().getLocation().reportSemanticError(MessageFormat.format(
									LOCALINHERITANCECOLLISION, definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
						}
					}
				} else if (extendsGainedDefinitions.containsKey(name)) {
					final Definition previousDefinition = extendsGainedDefinitions.get(name);
					if (!previousDefinition.equals(definition) && !previousDefinition.isInherited()) {
						// it is not the same definition inherited on two paths
						if (this.equals(previousDefinition.getMyScope())) {
							previousDefinition.getLocation().reportSemanticError(MessageFormat.format(LOCALINHERITANCECOLLISION,
									previousDefinition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
							definition.getIdentifier().getLocation().reportSemanticWarning(
									MessageFormat.format(INHERITEDLOCATION, definition.getIdentifier().getDisplayName()));
						} else if (identifier != null && identifier.getLocation() != null) {
							identifier.getLocation().reportSemanticError(MessageFormat.format(INHERITANCECOLLISION,
									definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName(), previousDefinition.getMyScope().getFullName()));

							definition.getIdentifier().getLocation().reportSingularSemanticWarning(MessageFormat.format(INHERITEDDEFINITIONLOCATION,
									definition.getIdentifier().getDisplayName(), definition.getMyScope().getFullName()));
							previousDefinition.getIdentifier().getLocation().reportSingularSemanticWarning(MessageFormat.format(INHERITEDDEFINITIONLOCATION,
									previousDefinition.getIdentifier().getDisplayName(), previousDefinition.getMyScope().getFullName()));
						}
					}
				} else {
					definition.setInherited(true);
					extendsGainedDefinitions.put(name, definition);

					if (!definition.getMyScope().getModuleScope().equals(parentScope.getModuleScope())) {
						if (parentScope.hasAssignmentWithId(timestamp, definition.getIdentifier())) {
							if (identifier != null && identifier.getLocation() != null) {
								identifier.getLocation().reportSemanticError(
										MessageFormat.format(HIDINGSCOPEELEMENT, definition.getIdentifier().getDisplayName()));
								final List<ISubReference> subReferences = new ArrayList<ISubReference>();
								subReferences.add(new FieldSubReference(definition.getIdentifier()));
								final Reference reference = new Reference(null, subReferences);
								final Assignment assignment = parentScope.getAssBySRef(timestamp, reference);
								if (assignment != null && assignment.getLocation() != null) {
									assignment.getLocation().reportSingularSemanticError(
											MessageFormat.format(HIDDENSCOPEELEMENT, definition.getIdentifier().getDisplayName()));
								}
								definition.getIdentifier().getLocation().reportSingularSemanticWarning(
										MessageFormat.format(INHERITEDDEFINITIONLOCATION, definition.getIdentifier().getDisplayName(),
												definition.getMyScope().getFullName()));
							}
						} else if (parentScope.isValidModuleId(definition.getIdentifier())) {
							definition.getLocation().reportSingularSemanticWarning(
									MessageFormat.format(HIDINGMODULEIDENTIFIER, definition.getIdentifier().getDisplayName()));
						}
					}
				}
			}
		}
	}
	
	/**
	 * Collect all parent class bodies that can be reached, recursively, via extends.
	 *
	 * @return the collected class type bodies.
	 * */
	public List<ClassTypeBody> getExtendsInheritedClassBodies(CompilationTimeStamp timestamp) {
		final List<ClassTypeBody> result = new ArrayList<ClassTypeBody>();
		extendsReferences.check(timestamp);
		final LinkedList<ClassTypeBody> toBeChecked = new LinkedList<ClassTypeBody>(extendsReferences.getClassBodies(timestamp));
		while(!toBeChecked.isEmpty()) {
			final ClassTypeBody body = toBeChecked.removeFirst();
			if(!result.contains(body)) {
				result.add(body);
				if (body.extendsReferences != null && body.extendsReferences.getClassBodies(null) != null) {
					for(final ClassTypeBody subBody : body.extendsReferences.getClassBodies(null)) {
						if(!result.contains(subBody) && !toBeChecked.contains(subBody)) {
							toBeChecked.add(subBody);
						}
					}
				}
			}
		}

		return result;
	}
	
	public Assignment getAssFromParents(final CompilationTimeStamp timestamp, final Reference reference) {
		final LinkedList<ClassTypeBody> toBeChecked = new LinkedList<ClassTypeBody>(extendsReferences.getClassBodies(null));
		
		Assignment assignment = null;
		for (ClassTypeBody body : toBeChecked) {
			assignment = body.getAssBySRef(timestamp, reference);
			if (assignment != null) {
				return assignment;
			}
		}
		return null;
	}
	
	public void check(final CompilationTimeStamp timestamp) {
		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		checkUniqueness(timestamp);
		
		definitions.checkAll(timestamp);
		
		for (Definition def : definitions.getDefinitions()) {
			if (! (def instanceof Def_Function)) {
				final VisibilityModifier visibility = def.getVisibilityModifier();
				if (visibility != VisibilityModifier.Private && visibility != VisibilityModifier.Protected) {
					def.getVisibilityModifierLocation().reportSemanticError(VISIBILITYPRIVATEPROTECTED);
				}
			}
		}
		
		if (myType.isAbstract()) {
			for (Definition def : definitions) {
				if (def instanceof Def_Function) {
					final Def_Function funcDef = (Def_Function)def;
					if (funcDef.isAbstract() && funcDef.hasBody()) {
						funcDef.getLocation().reportSingularSemanticError(ABSTRACTMETHODHASBODY);
						myType.setIsErroneous(true);
					}
				}
			}
		}
		if (myType.isTrait()) {
			for (Definition def : definitions) {
				if (def instanceof Def_Function) {
					final Def_Function funcDef = (Def_Function)def;
					if (isConstructor(funcDef)) {
						def.getLocation().reportSingularSemanticError(TRAITCONSTRUCTOR);
						myType.setIsErroneous(true);
					}
					else {
						if (! funcDef.isAbstract()) {
							def.getLocation().reportSingularSemanticError(TRAITMETHODABSTRACT);
						}
						if (funcDef.hasBody()) {
							def.getLocation().reportSingularSemanticError(TRAITMETHODHASBODY);
							myType.setIsErroneous(true);
						}
					}
				} else {
					def.getLocation().reportSingularSemanticError(TRAITMETHODSONLY);
					myType.setIsErroneous(true);
				}
			}
		}
	}
	
	private boolean isConstructor(Definition defFunction) {
		if (! (defFunction instanceof Def_Function))
			return false;
		
		return defFunction.getIdentifier().getName().equals("create");
	}
	
	public Identifier getIdentifier() {
		return identifier;
	}
	
	public Map<String, Definition> getDefinitionMap() {
		return definitions.getDefinitionMap();
	}
	
	/**
	 * Checks if a class has 'extends' references.
	 * @return true if the class has parents other than 'object', false otherwise.
	 */
	public boolean hasExtendsReferences() {
		return extendsReferences.hasExtendsReferences();
	}
	
	public ClassTypeReferenceList getExtendsReferences() {
		return extendsReferences;
	}
	
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		for (final Definition definition : definitions) {
			definition.updateSyntax(reparser, false);
			reparser.updateLocation(definition.getLocation());
			if(!definition.getLocation().equals(definition.getCumulativeDefinitionLocation())) {
				reparser.updateLocation(definition.getCumulativeDefinitionLocation());
			}
		}

		if (extendsReferences != null) {
			extendsReferences.updateSyntax(reparser, false);
			reparser.updateLocation(extendsReferences.getLocation());
		}
	}
	
	@Override
	public boolean accept(ASTVisitor v) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String chainedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Location getChainLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public Assignment getEnclosingAssignment(int offset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findReferences(ReferenceFinder referenceFinder, List<Hit> foundIdentifiers) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * checks whether the class has an explicit constructor
	 * @return
	 */
	public boolean hasExplicitConstructor() {
		return hasExplicitContructor;
	}
	
	public boolean hasConstructor() {
		for (Definition def : getOwnDefinitions()) {
			if (def instanceof Def_Function && def.getIdentifier().getName().equals("create")) {
				return true;
			}
		}
		return false;
	}
	
	public void addClassMembers(Assignment assignment, Ttcn3HoverContent content, DocumentComment dc) {
		content.addTag("Members:");
		Map<String,String> members = null;
		if (dc != null) {
			members = dc.getMembers();
		}
		for (Definition def : getDefinitions()) {
			switch (def.getVisibilityModifier()) {
			case Private:
				content.addText("<span style=\"margin-left: 20px; margin-right: 5px; color: red;\">\u25fc</span>");
				break;
			case Public:
				content.addText("<span style=\"margin-left: 20px; margin-right: 5px; color: green;\">\u25fc</span>");
				break;
			default:
				content.addText("<span style=\"margin-left: 20px; margin-right: 5px; color: orange;\">\u25fc</span>");
			}
			String id = def.getIdentifier().getDisplayName();
			String memberText = null;
			if (members != null) {
				memberText = members.get(id); 
			}
			String memberTypeName = null;
			if (def instanceof Def_Function) {
				if (id.equals("create")) {
					memberTypeName = "constructor";
				} else {
					memberTypeName = "function";
				}
			} else if (def instanceof Def_Timer) {
				memberTypeName = "timer";
			}
			else {
				final IType memberType = def.getType(lastCompilationTimeStamp);
				if (memberType != null) {
					memberTypeName = memberType.getTypename();
				}
			}
			content.addText(memberTypeName).addText(" ");
			content.addText("<b>" + def.getIdentifier().getDisplayName() + "</b>");
			content.addText(" ").addText(memberText != null ? memberText : "").addText("<br>");
		}
	}
	
	/** 
	 * returns the offset of the end of the last class definition 
	 * @return
	 */
	public int getDefinitionsLocation() {
		int maxoffset = getLocation().getOffset();
		
		for (Definition def : definitions.getDefinitions()) {
			if (def.getLocation().getEndOffset() > maxoffset) {
				maxoffset = def.getLocation().getEndOffset();
			}
		}
		
		return maxoffset + 1;
	}
}