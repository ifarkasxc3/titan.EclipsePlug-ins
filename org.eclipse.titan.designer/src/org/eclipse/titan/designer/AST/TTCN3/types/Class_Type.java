/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.designer.AST.TTCN3.types;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.FieldSubReference;
import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.ISubReference;
import org.eclipse.titan.designer.AST.ISubReference.Subreference_type;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.ITypeWithComponents;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NULL_Location;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.ParameterisedSubReference;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Reference.Ref_Type;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TypeCompatibilityInfo;
import org.eclipse.titan.designer.AST.TypeCompatibilityInfo.Chain;
import org.eclipse.titan.designer.AST.Value;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Const;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Template;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var_Template;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.definitions.VisibilityModifier;
import org.eclipse.titan.designer.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.designer.AST.TTCN3.templates.SpecificValue_Template;
import org.eclipse.titan.designer.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.designer.AST.TTCN3.templates.TemplateInstance;
import org.eclipse.titan.designer.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents the TTCN3 class type (TTCN-3 extension).
 *
 * @author Miklos Magyari
 * */
public final class Class_Type extends Type implements ITypeWithComponents {
	private static final String ABSTRACTCANNOTBEFINAL = "An absract class cannot be final";
	private static final String TRAITCANNOTBEFINAL = "A trait class cannot be final";
	private static final String ABSTRACTCANNOTBETRAIT = "A class cannot be both abstract and trait";
	private static final String TRAITWITHFINALLY = "A trait class should not define a finally block";
	private static final String UNKNOWNFIELD = "Unknown field reference";
	private static final String PRIVATEINACCESSIBLE = "Private member is inaccessible due to its protection level";
	
	/** 
	 * The superclass of this class
	 * (the only non-trait class that this class extends) 
	 **/
	private Class_Type superClass = null;
	
	private final ClassTypeBody classBody;
	
	private final boolean isAbstract;
	private final boolean isFinal;
	private final boolean isTrait;
	private final boolean isExternal;
	private final Location modifierLocation;
	private final StatementBlock finallyBlock;
	private final Reference runsOnRef;
	private final Reference mtcRef;
	private final Reference systemRef;
	private final ClassTypeReferenceList refs;
	
	private Component_Type runsOnType;
	private Component_Type mtcType;
	private Component_Type systemType;
	
	public enum ClassRelation {
		Identical, Related, Unrelated 
	}
	
	public Class_Type(ClassTypeBody classBody, boolean isAbstract, boolean isFinal, boolean isTrait, boolean isExternal, 
		Location modifierLocation, final Reference runsOnRef, final Reference mtcRef, final Reference systemRef,
		ClassTypeReferenceList refs, StatementBlock finallyBlock) {
		
		this.classBody = classBody;
		this.isAbstract = isAbstract;
		this.isFinal = isFinal;
		this.isTrait = isTrait;
		this.isExternal = isExternal;
		this.modifierLocation = modifierLocation;
		this.finallyBlock = finallyBlock;
		this.runsOnRef = runsOnRef;
		this.mtcRef = mtcRef;
		this.systemRef = systemRef;
		this.refs = refs;
		
		if (classBody != null) {
			classBody.setFullNameParent(this);
			classBody.setMyType(this);
		}
		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (mtcRef != null) {
			mtcRef.setFullNameParent(this);
		}
		if (systemRef != null) {
			systemRef.setFullNameParent(this);
		}
		if (finallyBlock != null) {
			finallyBlock.setFullNameParent(this);
			finallyBlock.setOwnerIsFinally();
		}
	}
	
	/** 
	 * constructor used for classes defined using the 'object' keyword 
	 */
	public Class_Type() {
		this(null, true, false, false, false, null, null, null, null, null, null);
	}
	
	@Override
	public Type_type getTypetype() {
		return Type_type.TYPE_CLASS;
	}
	
	 @Override
    /** {@inheritDoc} */
    public Type_type getTypetypeTtcn3() {
    	if (isErroneous) {
    		return Type_type.TYPE_UNDEFINED;
    	}
    	return getTypetype();
    }
	
	 @Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (classBody != null) {
			classBody.setMyScope(scope);
			if (finallyBlock != null) {
				finallyBlock.setMyScope(classBody);
			}
		} else {
			if (finallyBlock != null) {
				finallyBlock.setMyScope(scope);
			}
		}
		if (runsOnRef != null) {
			runsOnRef.setMyScope(scope);
		}
		if (mtcRef != null) {
			mtcRef.setMyScope(scope);
		}
		if (systemRef != null) {
			systemRef.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public IType getFieldType(final CompilationTimeStamp timestamp, final Reference reference, final int actualSubReference,
			final Expected_Value_type expectedIndex, final IReferenceChain refChain, final boolean interruptIfOptional) {
		final List<ISubReference> subreferences = reference.getSubreferences();
		if (subreferences.size() <= actualSubReference) {
			return this;
		}

		final ISubReference subreference = subreferences.get(actualSubReference);		
		
		Assignment assignment = null;
		if (classBody != null) {
			assignment = classBody.getAssByIdentifier(timestamp, subreference.getId());
		}
		if (assignment == null) {
			/* methods defined by 'object' */
			if (subreference.getReferenceType() == Subreference_type.parameterisedSubReference) {
				if (subreference.getId().getName().equals("toString")) {
					Identifier id = new Identifier(Identifier_type.ID_NAME, "toString");
					Def_Function function = new Def_Function(id, new FormalParameterList(new ArrayList<FormalParameter>()), new UniversalCharstring_Type());
					function.setMyScope(reference.getMyScope());
					function.setLocation(NULL_Location.INSTANCE);
					return function.getType(timestamp);
				}
				if (subreference.getId().getName().equals("equals")) {
					Identifier id = new Identifier(Identifier_type.ID_NAME, "equals");
					Identifier objid = new Identifier(Identifier_type.ID_NAME, "obj");
					List<FormalParameter> list = new ArrayList<FormalParameter>();
					list.add(new FormalParameter(null, Assignment_type.A_PAR_VAL_IN,
							new Anytype_Type(), objid, null, null));
					FormalParameterList paramList = new FormalParameterList(list);
					Def_Function function = new Def_Function(id, paramList, new Boolean_Type());
					function.setMyScope(reference.getMyScope());
					function.setLocation(NULL_Location.INSTANCE);
					return function.getType(timestamp);
				}
			}
			subreference.getLocation().reportSemanticError(UNKNOWNFIELD);
			return null;
		}
		final Definition def = (Definition)assignment;
		if (def.getVisibilityModifier() == VisibilityModifier.Private) {
			subreference.getLocation().reportSemanticError(PRIVATEINACCESSIBLE);
			return null;
		}
				
		if (assignment instanceof Def_Function) {
			final FormalParameterList fpl = ((Def_Function)assignment).getFormalParameterList();
			final ISubReference sr = reference.getSubreferences().get(actualSubReference);
			if (sr instanceof ParameterisedSubReference) {
				((ParameterisedSubReference)sr).checkParameters(timestamp, fpl);
			}
		}
		
		if (subreferences.size() == actualSubReference + 1) {
			return assignment.getType(timestamp);
		}
		
 		Type type = (Type)assignment.getType(timestamp);
		return (Type)type.getFieldType(timestamp, reference, actualSubReference + 1, expectedIndex, interruptIfOptional);
	}
	
	public ClassTypeBody getClassBody() {
		return classBody;
	}
	
	public Reference getRunsOnRef() {
		return runsOnRef;
	}
	
	public Reference getMtcRef() {
		return mtcRef;
	}
	
	public Reference getSystemRef() {
		return systemRef;
	}
	
	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		
		if (refs != null) {
			for (Reference ref : refs.getReferenceList()) {
				final Assignment assignment = ref.getRefdAssignment(timestamp, false);
				if (assignment != null) {
					final IType type = assignment.getType(timestamp);
					if (type instanceof Class_Type) {
						final Class_Type parent = (Class_Type)type;
						if (parent.isTrait == false) {
							superClass = parent;
						}
					}
				}
			}
		}
		runsOnType = null;
		mtcType = null;
		systemType = null;
		if (runsOnRef != null) {
			runsOnType = runsOnRef.chkComponentypeReference(timestamp);
		}
		if (mtcRef != null) {
			mtcType = mtcRef.chkComponentypeReference(timestamp);
		}
		if (systemRef != null) {
			systemType = systemRef.chkComponentypeReference(timestamp);
		}
		
		if (isAbstract && isFinal) {
			modifierLocation.reportSemanticError(ABSTRACTCANNOTBEFINAL);
			setIsErroneous(true);
		}
		
		if (isTrait && isFinal) {
			modifierLocation.reportSemanticError(TRAITCANNOTBEFINAL);
			setIsErroneous(true);
		}
		
		if (isAbstract && isTrait) {
			modifierLocation.reportSemanticError(ABSTRACTCANNOTBETRAIT);
			setIsErroneous(true);
		}
		
		if (isTrait && finallyBlock != null) {
			finallyBlock.getLocation().reportSemanticError(TRAITWITHFINALLY);
			setIsErroneous(true);
		}

		initAttributes(timestamp);
		
		if (classBody != null) {
			if (classBody.hasExplicitConstructor() == true || classBody.hasConstructor()) {
				// hasContructor = true;
			} else {
				// add default implicit constructor
				final List<FormalParameter> fpList = new ArrayList<FormalParameter>();
				final StatementBlock sb = new StatementBlock();
				for (final Definition def : classBody.getOwnDefinitions()) {
					if (def instanceof Def_Var || def instanceof Def_Var_Template ||
						def instanceof Def_Const || def instanceof Def_Template) {
 						boolean isTemplate = false;
						Value defval = null;
						if (def instanceof Def_Template || def instanceof Def_Var_Template) {
							isTemplate = true;
						}
						if (def instanceof Def_Const) {
							final Def_Const con = (Def_Const)def;
							if (con.getValue() != null) {
								continue;
							}
						}
						if (def instanceof Def_Template) {
							final Def_Template tpl = (Def_Template)def;
							if (tpl.getTemplate(timestamp) != null) {
								continue;
							}
						}
						
						TTCN3Template template = null;
						if (def instanceof Def_Var_Template) {
							template = ((Def_Var_Template)def).getInitialValue();
						}
						if (def instanceof Def_Var) {
							defval = ((Def_Var) def).getInitialValue();
							if (defval != null) {
								template = new SpecificValue_Template(defval);
							}
						}
						TemplateInstance instance = null;
						if (template != null) {
							instance = new TemplateInstance(null, null, template);
						}
						
						final IType deftype = def.getType(timestamp);
						if (deftype instanceof Property_Type) {
							if (((Property_Type)deftype).isInternal()) {
								continue;
							}
						}
						String name = def.getIdentifier().getName();
						if (! name.startsWith("pl_")) {
							name = "pl_" + name;
						}
						final FormalParameter fp = new FormalParameter(Restriction_type.TR_NONE, 
							isTemplate ? Assignment_type.A_PAR_TEMP_IN : Assignment_type.A_PAR_VAL_IN,  
							(Type)def.getType(timestamp), new Identifier(Identifier_type.ID_TTCN, name), 
							instance, null);
						fpList.add(fp);
						final Reference refLeft = new Reference(new Identifier(Identifier_type.ID_TTCN, def.getIdentifier().getName()), Ref_Type.REF_THIS);
						final FieldSubReference subref = new FieldSubReference(def.getIdentifier());
						refLeft.addSubReference(subref);
						final Reference refRight = new Reference(new Identifier(Identifier_type.ID_TTCN, def.getIdentifier().getName()));
						final Referenced_Value refdVal = new Referenced_Value(refRight);
						final SpecificValue_Template valTemplate = new SpecificValue_Template(refdVal);
						final Assignment_Statement assignment = new Assignment_Statement(refLeft, valTemplate);
						sb.addStatement(assignment);
					}
				}
				FormalParameterList pl = null;
				if (superClass != null) {
					final FormalParameterList parentFpList = superClass.getConstructorFormalParameterList();
					if (parentFpList != null) {
						pl = new FormalParameterList(parentFpList);
						pl.addFormalParameterList(new FormalParameterList(fpList));
					}
				}
				if (pl == null) {
					pl = new FormalParameterList(fpList);
				}
				final Def_Function constructor = new Def_Function(new Identifier(Identifier_type.ID_TTCN, "create"), pl, null);
				constructor.setLocation(NULL_Location.INSTANCE);
				classBody.addDefinition(constructor);
			}
		}
		
		if (classBody != null) {
			classBody.check(timestamp);
		}
		
		if (finallyBlock != null) {
			finallyBlock.check(timestamp);
		}

		lastTimeChecked = timestamp;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;
			boolean handled = false;
			
			if (classBody != null && classBody.getLocation() != null) {
				if (reparser.envelopsDamage(classBody.getLocation())) {
					classBody.updateSyntax(reparser, true);
					reparser.updateLocation(classBody.getLocation());
					handled = true;
				}
			}
			
			if (subType != null) {
				subType.updateSyntax(reparser, false);
				handled = true;
			}
			
			if (handled) {
				return;
			}
			
			throw new ReParseException();
		}
		
		if (classBody != null) {
			classBody.updateSyntax(reparser, false);
			reparser.updateLocation(classBody.getLocation());
		}
		
		if (subType != null) {
			subType.updateSyntax(reparser, false);
		}
		
		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}
	
	@Override
	public String getOutlineIcon() {
		return "class.gif";
	}
	
	@Override
	/** {@inheritDoc} */
	public Object[] getOutlineChildren() {
		return classBody.getDefinitions().toArray();
	}

	@Override
	public String getTypename() {
		return getFullName();
	}
	
	/** 
	 * checks if a class has any parents other than 'object'
	 * @return
	 */
	public boolean hasRealParent() {
		return classBody.hasExtendsReferences();
	}
	
	/**
	 * Checks if the class is declared as external
	 * @return
	 */
	public boolean isExternal() {
		return isExternal;
	}
	
	/**
	 * Checks if the class is declared using the '@trait' modifier
	 * @return
	 */
	public boolean isTrait() {
		return isTrait;
	}
	
	/**
	 * Checks if the class is declared using the '@abstract' modifier
	 * @return
	 */
	public boolean isAbstract() {
		return isAbstract;
	}
	
	/**
	 * Checks if the class is declared using the '@final' modifier
	 * @return
	 */
	public boolean isFinal() {
		return isFinal;
	}
	
	/** 
	 * returns true for classes defined with the 'object' keyword 
	 *   
	 * @return
	 */
	public boolean isObject() {
		return classBody == null;
	}
	
	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return runsOnType;
	}
	
	public Component_Type getMtcType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return mtcType;
	}
	
	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return systemType;
	}
	
	@Override
	public Identifier getComponentIdentifierByName(Identifier identifier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkThisTemplate(CompilationTimeStamp timestamp, ITTCN3Template template, boolean isModified,
			boolean implicitOmit, Assignment lhs) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCompatible(CompilationTimeStamp timestamp, IType otherType, TypeCompatibilityInfo info,
			Chain leftChain, Chain rightChain) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean generatesOwnClass(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return needsAlias();
	}

	@Override
	public void generateCode(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getGenNameValue(JavaGenData aData, StringBuilder source) {
		return "TitanClass";
	}

	@Override
	public String getGenNameTemplate(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGenNameTypeDescriptor(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/** Checks whether a class is a direct or indirect superclass/subclass of this class
	 * 
	 * @param class to be checked
	 * @return
	 */
	public ClassRelation isRelatedClass(CompilationTimeStamp timestamp, Class_Type related) {
		if (this.isObject()) {
			if (related.isObject()) {
				return ClassRelation.Identical;
			} else {
				return ClassRelation.Related;
			}
		}
		
		if (related.isObject()) {
			// 'Identical' is handled by the previous `if` block 
			return ClassRelation.Related;
		}
		
		if (this.equals(related)) {
			return ClassRelation.Identical;
		}
		getClassBody().check(timestamp);
		List<ClassTypeBody> bodies = getClassBody().getExtendsInheritedClassBodies(timestamp);
		for (ClassTypeBody body : bodies) {
			if (body.getMyType() == related) {
				return ClassRelation.Related;
			}		
		}
		related.getClassBody().check(timestamp);
		List<ClassTypeBody> relatedBodies = related.getClassBody().getExtendsInheritedClassBodies(timestamp);
		for (ClassTypeBody body : relatedBodies) {
			if (body.getMyType() == this) {
				return ClassRelation.Related;
			}		
		}
		return ClassRelation.Unrelated;
	}
	
	private FormalParameterList getConstructorFormalParameterList() {
		if (classBody == null) {
			return null;
		}
		for (Definition def : classBody.getOwnDefinitions()) {
			if (def instanceof Def_Function) {
				if (def.getIdentifier().getName().equals("create")) {
					return ((Def_Function)def).getFormalParameterList();
				}
			}
		}
		return null;
	}
	
	/**
	 * Tries to find an enclosing class body, if exists
	 * 
	 * @param scope
	 * @return the enclosing class body, or null if the scope is not inside a class
	 */
	public static ClassTypeBody getEnclosingClassBody(Scope scope) {
		do {
			scope = scope.getParentScope();
		} while (scope instanceof StatementBlock || scope instanceof FormalParameterList || scope instanceof NamedBridgeScope);
		
		return scope instanceof ClassTypeBody ? (ClassTypeBody)scope : null;
	}
}