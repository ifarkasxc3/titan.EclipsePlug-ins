/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * UndefCreateExpression represents an expression in the source code that is 
 * either a create operation or a constructor call.
 * 
 * Ttcn grammar is ambiguous for some cases and the parser is unable to 
 * differentiate between createop and constructor call in all circumstances 
 * so we need to store parsed data in a temporary form to postpone the decision 
 * for the semantic analysis phase.
 *  
 * @author Miklos Magyari
 */

package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.INamedNode;
import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.Value;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ActualParameterList;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.templates.ParsedActualParameters;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

public class UndefCreateExpression extends Expression_Value {
	private static final String FIRSTOPERANDERROR = "The first operand of operation `create()' should be a charstring value";
	private static final String SECONDOPERANDERROR = "The second operand of operation `create()' should be a charstring value";
	private static final String COMPONENTEXPECTED = "Operation `create'' should refer to a component type instead of {0}";
	private static final String TYPEMISMATCH1 = "Type mismatch: reference to a component type was expected in operation `create'' instead of `{0}''";
	private static final String TYPEMISMATCH2 = "Incompatible component type: operation `create'' should refer to `{0}'' instaed of `{1}''";
	private static final String TRAITINSTANTIATED = "A trait class cannot be instantiated";
	private static final String ABSTRACTINSTANTIATED = "An abstract class cannot be instantiated";
	private static final String OPERATIONNAME = "create()";
	
	final private Reference componentReference;
	final private Value name;
	final private Value location;
	final private boolean isAlive;
	final private ParsedActualParameters parameters;
	
	private Expression_Value realExpression;
	
	private CompilationTimeStamp checkCreateTimestamp;
	private Type checkCreateCache;
	
	public UndefCreateExpression(final Reference reference, final Value name, final Value location, 
								 final boolean isAlive, final ParsedActualParameters parameters) {
		this.componentReference = reference;
		this.name = name;
		this.location = location;
		this.isAlive = isAlive;
		this.parameters = parameters;
		
		if (reference != null) {
			reference.setFullNameParent(this);
		}
		if (name != null) {
			name.setFullNameParent(this);
		}
		if (location != null) {
			location.setFullNameParent(this);
		}
	}
	
	public UndefCreateExpression(final Reference reference, final Value name, final Value location, final boolean isAlive) {
		this(reference, name, location, isAlive, null);
	}
	
	public UndefCreateExpression(final Reference reference, final ParsedActualParameters parameters) {
		this(reference, null, null, false, parameters);
	}
	
	@Override
	/** {@inheritDoc} */
	public Operation_type getOperationType() {
		return Operation_type.UNDEFINED_CREATE_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReference(final CompilationTimeStamp timestamp, final Assignment lhs) {
		// assume no self-ref
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (componentReference != null) {
			componentReference.setMyScope(scope);
		}
		if (name != null) {
			name.setMyScope(scope);
		}
		if (location != null) {
			location.setMyScope(scope);
		}
		if (parameters != null) {
			parameters.setMyScope(scope);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void setCodeSection(final CodeSectionType codeSection) {
		super.setCodeSection(codeSection);

		if (componentReference != null) {
			componentReference.setCodeSection(codeSection);
		}
		if (name != null) {
			name.setCodeSection(codeSection);
		}
		if (location != null) {
			location.setCodeSection(codeSection);
		}
		if (realExpression != null) {
			realExpression.setCodeSection(codeSection);
		}
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (componentReference == child) {
			return builder.append(OPERAND1);
		} else if (name == child) {
			return builder.append(OPERAND2);
		} else if (location == child) {
			return builder.append(OPERAND3);
		}

		return builder;
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (realExpression != null) {
			return realExpression.getExpressionReturntype(timestamp, expectedValue);
		} 
		
		return null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean isUnfoldable(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Type getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		return checkCreate(timestamp);
	}

	/**
	 * Checks the parameters of the expression and if they are valid in
	 * their position in the expression or not.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param expectedValue
	 *                the kind of value expected.
	 * @param referenceChain
	 *                a reference chain to detect cyclic references.
	 * */
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		setIsErroneous(false);

		checkCreate(timestamp);
		if (componentReference == null) {
			return;
		}

		final Assignment assignment = componentReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return;
		}
		
		IType assignmentType = assignment.getType(timestamp);
		if (assignmentType instanceof Component_Type) {
			if (name != null) {
				final IValue last = name.setLoweridToReference(timestamp);
				final Type_type typeType = last.getExpressionReturntype(timestamp, expectedValue);
				if (!last.getIsErroneous(timestamp)) {
					switch (typeType) {
					case TYPE_CHARSTRING:
						last.getValueRefdLast(timestamp, referenceChain);
						break;
					case TYPE_UNDEFINED:
						break;
					default:
						name.getLocation().reportSemanticError(FIRSTOPERANDERROR);
						setIsErroneous(true);
						break;
					}
				}
			}
			if (location != null) {
				final IValue last = location.setLoweridToReference(timestamp);
				final Type_type typeType = last.getExpressionReturntype(timestamp, expectedValue);
				if (!last.getIsErroneous(timestamp)) {
					switch (typeType) {
					case TYPE_CHARSTRING:
						last.getValueRefdLast(timestamp, referenceChain);
						break;
					case TYPE_UNDEFINED:
						break;
					default:
						name.getLocation().reportSemanticError(SECONDOPERANDERROR);
						setIsErroneous(true);
						break;
					}
				}
			}
			realExpression = new ComponentCreateExpression(componentReference, name, location, isAlive);
			realExpression.setMyScope(getMyScope());
			realExpression.setFullNameParent(this);
			realExpression.setLocation(getLocation());
			realExpression.evaluateValue(timestamp, expectedValue, referenceChain);
			checkExpressionDynamicPart(expectedValue, OPERATIONNAME, false, true, false);
		} else if (assignmentType instanceof Class_Type) {
			final Class_Type classInstance = (Class_Type)assignmentType;
			if (classInstance.isTrait()) {
				getLocation().reportSemanticError(TRAITINSTANTIATED);
				setIsErroneous(true);
			}
			if (classInstance.isAbstract()) {
				getLocation().reportSemanticError(ABSTRACTINSTANTIATED);
				setIsErroneous(true);
			}
			realExpression = new ClassConstructorExpression(componentReference, parameters);
			realExpression.setMyScope(getMyScope());
			realExpression.setFullNameParent(this);
			realExpression.setLocation(getLocation());
			realExpression.evaluateValue(timestamp, expectedValue, referenceChain);
			
			final ActualParameterList tempActualParameters = new ActualParameterList();
			final Assignment constructorAssignment = classInstance.getClassBody().getAssByIdentifier(timestamp, new Identifier(Identifier_type.ID_TTCN, "create"));
			if (constructorAssignment != null) {
				FormalParameterList fpList = ((Def_Function)constructorAssignment).getFormalParameterList();
				fpList.checkActualParameterList(timestamp, parameters, tempActualParameters);
			}
			// FIXME : implement
		} 
	}

	@Override
	/** {@inheritDoc} */
	public IValue evaluateValue(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}
		
		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;
		
		if (componentReference == null) {
			return lastValue;
		}
		
		checkExpressionOperands(timestamp, expectedValue, referenceChain);
		checkCreate(timestamp);
		return lastValue;
	}

	private Type checkCreate(final CompilationTimeStamp timestamp) {
		if (checkCreateTimestamp != null && !checkCreateTimestamp.isLess(timestamp)) {
			return checkCreateCache;
		}

		checkCreateTimestamp = timestamp;
		checkCreateCache = null;

		final Assignment assignment = componentReference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			setIsErroneous(true);
			return null;
		}

		if (!Assignment_type.A_TYPE.semanticallyEquals(assignment.getAssignmentType())) {
			componentReference.getLocation().reportSemanticError(MessageFormat.format(COMPONENTEXPECTED, assignment.getDescription()));
			setIsErroneous(true);
			return null;
		}

		final IType type = ((Def_Type) assignment).getType(timestamp).getFieldType(timestamp, componentReference, 1,
				Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
		if (type == null) {
			setIsErroneous(true);
			return null;
		}

		boolean isComponent = true;
		IType assignmentType = assignment.getType(timestamp);
		if (assignmentType instanceof Component_Type) {
			isComponent = true;
		}
		else if (assignmentType instanceof Class_Type) {
			isComponent = false;
		} else {
			if (!Type_type.TYPE_COMPONENT.equals(type.getTypetype())) {
				componentReference.getLocation().reportSemanticError(MessageFormat.format(TYPEMISMATCH1, type.getTypename()));
				setIsErroneous(true);
				return null;
			}
		}
		
		if (myGovernor != null) {
			final IType last = myGovernor.getTypeRefdLast(timestamp);

			if (Type_type.TYPE_COMPONENT.equals(last.getTypetype()) && !last.isCompatible(timestamp, type, null, null, null)) {
				componentReference.getLocation().reportSemanticError(
						MessageFormat.format(TYPEMISMATCH2, last.getTypename(), type.getTypename()));
				setIsErroneous(true);
				return null;
			}
		}

		if (isComponent == true) {
			checkCreateCache = (Component_Type) type;
		} else {
			checkCreateCache = (Class_Type) type;
		}
		
		return checkCreateCache;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		if (componentReference != null) {
			componentReference.updateSyntax(reparser, false);
			reparser.updateLocation(componentReference.getLocation());
		}

		if (name != null) {
			name.updateSyntax(reparser, false);
			reparser.updateLocation(name.getLocation());
		}

		if (location != null) {
			location.updateSyntax(reparser, false);
			reparser.updateLocation(location.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (componentReference != null) {
			componentReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (name != null) {
			name.findReferences(referenceFinder, foundIdentifiers);
		}
		if (location != null) {
			location.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (componentReference != null && !componentReference.accept(v)) {
			return false;
		}
		if (name != null && !name.accept(v)) {
			return false;
		}
		if (location != null && !location.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean returnsNative() {
		if (realExpression != null && realExpression != this) {
			return realExpression.returnsNative();
		}

		return false;
	}
	
	@Override
	/** {@inheritDoc} */
	public StringBuilder generateSingleExpression(final JavaGenData aData) {
		if (realExpression != null) {
			return realExpression.generateSingleExpression(aData);
		}

		return new StringBuilder();
	}

	@Override
	/** {@inheritDoc} */
	public void generateCodeExpressionExpression(final JavaGenData aData, final ExpressionStruct expression) {
		if (realExpression != null) {
			realExpression.generateCodeExpressionExpression(aData, expression);
		}
	}
	
	@Override
	public String createStringRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}
}
