/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension3;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension6;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;

/**
 * The default class for a single completion proposal.
 * <p>
 * For skeleton like proposals the templatecompletion should be used.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class CompletionProposal implements ICompletionProposal, ICompletionProposalExtension3, ICompletionProposalExtension6 {

	/** The styled string to be displayed in the completion proposal popup. */
	private StyledString fDisplayString;
	/** The replacement string. */
	private String fReplacementString;
	/** The replacement offset. */
	private int fReplacementOffset;
	/** The replacement length. */
	private int fReplacementLength;
	/** The cursor position after this proposal has been applied. */
	private int fCursorPosition;
	/** The image to be displayed in the completion proposal popup. */
	private Image fImage;
	/** The context information of this proposal. */
	private IContextInformation fContextInformation;
	/** The additional info of this proposal. */
	private Ttcn3HoverContent fAdditionalProposalInfo;
	/** Relevance of the proposal, used for sorting. */
	private int fRelevance;

	/**
	 * Creates a new completion proposal based on the provided information.
	 * The replacement string is considered being the display string too.
	 * All remaining fields are set to <code>null</code>.
	 *
	 * @param replacementString
	 *                the actual string to be inserted into the document
	 * @param replacementOffset
	 *                the offset of the text to be replaced
	 * @param replacementLength
	 *                the length of the text to be replaced
	 * @param cursorPosition
	 *                the position of the cursor following the insert
	 *                relative to replacementOffset
	 */
	public CompletionProposal(final String replacementString, final int replacementOffset, final int replacementLength, final int cursorPosition) {
		this(replacementString, replacementOffset, replacementLength, cursorPosition, null, null, null, null);
	}

	/**
	 * Creates a new completion proposal. All fields are initialized based
	 * on the provided information.
	 *
	 * @param replacementString
	 *                the actual string to be inserted into the document
	 * @param replacementOffset
	 *                the offset of the text to be replaced
	 * @param replacementLength
	 *                the length of the text to be replaced
	 * @param cursorPosition
	 *                the position of the cursor following the insert
	 *                relative to replacementOffset
	 * @param image
	 *                the image to display for this proposal
	 * @param displayString
	 *                the string to be displayed for the proposal
	 * @param contextInformation
	 *                the context information associated with this proposal
	 * @param additionalProposalInfo
	 *                the additional information associated with this
	 *                proposal
	 * @param relevance
	 *                the relevance
	 */
	public CompletionProposal(final String replacementString, final int replacementOffset, final int replacementLength, final int cursorPosition,
			final Image image, final String displayString, final IContextInformation contextInformation,
			final String additionalProposalInfo) {
		this(replacementString, replacementOffset, replacementLength, cursorPosition, image, new StyledString(displayString),
				contextInformation, new Ttcn3HoverContent(additionalProposalInfo), 0);
	}
	/**
	 * Creates a new completion proposal. All fields are initialized based
	 * on the provided information.
	 *
	 * @param replacementString
	 *                the actual string to be inserted into the document
	 * @param replacementOffset
	 *                the offset of the text to be replaced
	 * @param replacementLength
	 *                the length of the text to be replaced
	 * @param cursorPosition
	 *                the position of the cursor following the insert
	 *                relative to replacementOffset
	 * @param image
	 *                the image to display for this proposal
	 * @param displayString
	 *                the string to be displayed for the proposal
	 * @param contextInformation
	 *                the context information associated with this proposal
	 * @param additionalProposalInfo
	 *                the additional information associated with this
	 *                proposal
	 * @param relevance
	 *                the relevance
	 */
	public CompletionProposal(final String replacementString, final int replacementOffset, final int replacementLength, final int cursorPosition,
			final Image image, final StyledString displayString, final IContextInformation contextInformation,
			final Ttcn3HoverContent additionalProposalInfo, final int relevance) {
		Assert.isNotNull(replacementString);
		Assert.isTrue(replacementOffset >= 0);
		Assert.isTrue(replacementLength >= 0);
		Assert.isTrue(cursorPosition >= 0);

		setReplacementString(replacementString);
		setReplacementOffset(replacementOffset);
		setReplacementLength(replacementLength);
		setCursorPosition(cursorPosition);
		setImage(image);
		setStyledDisplayString(displayString == null ? new StyledString(replacementString) : displayString);
		setContextInformation(contextInformation);
		setAdditionalProposalInfo(additionalProposalInfo);
		setRelevance(relevance);
	}

	@Override
	public void apply(final IDocument document) {
		try {
			document.replace(fReplacementOffset, fReplacementLength, fReplacementString);
		} catch (BadLocationException x) {
			// ignore
		}
	}

	@Override
	public String getAdditionalProposalInfo() {
		if (fAdditionalProposalInfo != null) {
			return fAdditionalProposalInfo.getText(HoverContentType.INFO);
		}
		return "";
	}

	public void setAdditionalProposalInfo(final Ttcn3HoverContent additionalProposalInfo) {
		fAdditionalProposalInfo = additionalProposalInfo;
	}

	@Override
	public IContextInformation getContextInformation() {
		return fContextInformation;
	}

	public void setContextInformation(final IContextInformation contextInformation) {
		fContextInformation = contextInformation;
	}

	public void setCursorPosition(final int cursorPosition) {
		Assert.isTrue(cursorPosition >= 0);
		fCursorPosition = cursorPosition;
	}

	@Override
	public Image getImage() {
		return fImage;
	}

	public void setImage(final Image image) {
		fImage = image;
	}

	public int getRelevance() {
		return fRelevance;
	}

	public void setRelevance(final int relevance) {
		fRelevance = relevance;
	}

	public void setReplacementLength(final int replacementLength) {
		Assert.isTrue(replacementLength >= 0);
		fReplacementLength = replacementLength;
	}

	public void setReplacementOffset(final int replacementOffset) {
		Assert.isTrue(replacementOffset >= 0);
		fReplacementOffset = replacementOffset;
	}

	public void setReplacementString(final String replacementString) {
		Assert.isNotNull(replacementString);
		fReplacementString = replacementString;
	}

	@Override
	public String getDisplayString() {
		if (fDisplayString != null) {
			return fDisplayString.toString();
		}
		return fReplacementString;
	}

	@Override
	public StyledString getStyledDisplayString() {
		return fDisplayString;
	}

	public void setStyledDisplayString(final StyledString text) {
		fDisplayString = text;
	}

	@Override
	public IInformationControlCreator getInformationControlCreator() {
		return new IInformationControlCreator() {
            @Override
            public IInformationControl createInformationControl(Shell parent) {
            	return new Ttcn3HoverInfoControl(parent);
            }
        };
	}

	@Override
	public int getPrefixCompletionStart(final IDocument document, final int completionOffset) {
		return fReplacementOffset;
	}

	@Override
	public CharSequence getPrefixCompletionText(final IDocument document, final int completionOffset) {
		return fReplacementString;
	}

	@Override
	public Point getSelection(final IDocument document) {
		return new Point(fReplacementOffset + fCursorPosition, 0);
	}
}
