/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.text.MessageFormat;

import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.AbstractInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IInformationControlExtension2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;

/** 
 * Control for handling ttcn3 source editor hovers and code completion proposal info
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 *
 */
public class Ttcn3HoverInfoControl extends AbstractInformationControl implements IInformationControlExtension2 {
	/** Default status text in the hover window */
	private static final String STATUS_TEXT = "Hover to focus";
	/** Text of view switch button */
	private static final String SWITCHBUTTON_TEXT_PATTERN = "Click to switch to {0} view";
	/** Width/height of the scrollbar */
	private static final int scrollBarSize = 20;
	/** The vertical margin when laying out the information control
	 *  @see org.eclipse.jface.text.AbstractInformationControlManager#fMarginY */
	private static final int fMarginY= 5;

	/** The control's browser widget */
	private Browser browser;
	private StyledText textViewer;
	private ToolBarManager toolbar;
	private Composite fParent;
	private ToolItem switchButton;

	// Default size for hover window
	private static int lastWidth = 400;
	private static int lastHeight = 200;
	private int toolbarHeight = 0;
	private int toolbarWidth = 0;

	private Ttcn3HoverContent content;
	private MarkerHoverContent markerContent;
	private HoverContentType actualType = HoverContentType.loadAsProperty();
	
	private boolean isProposalContent;

	/**
	 * Selection listener for the view switch button
	 */
	private SelectionListener switchListener = new SelectionListener() {
		@Override
		public void widgetSelected(SelectionEvent e) {
			switch (actualType) {
			case INFO:
				actualType = HoverContentType.SOURCE;
				break;
			case SOURCE:
				actualType = HoverContentType.INFO;
				break;
			}
			if (e.widget instanceof ToolItem) {
				((ToolItem)e.widget).setText(getButtonText());
				HoverContentType.storeAsProperty(actualType);
				lastWidth = getShell().getSize().x;
				lastHeight = getShell().getSize().y;
				handleTtcn3HoverContent(false);
				toolbar.update(false);
			}
		}
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Do nothing
		}
	};

	/**
	 * Focus listener to store the size of the hover window.
	 * If the user changes the size of the hover window, it will be used for the next time.
	 */
	private FocusListener focusListenerToStoreSize = new FocusListener() {
		@Override
		public void focusLost(FocusEvent e) {
			lastWidth = getShell().getClientArea().width;
			lastHeight = getShell().getClientArea().height - fMarginY;
		}
		@Override
		public void focusGained(FocusEvent e) {
			// Do  nothing
		}
	};

	/**
	 * Focus listener to dispose this window.
	 */
	private FocusListener focusListenerToDispose = new FocusListener() {
		@Override
		public void focusLost(FocusEvent e) {
			dispose();
		}
		@Override
		public void focusGained(FocusEvent e) {
			// Do nothing
		}
	};

	/**
	 * Called when hovering over source code or over a marker
	 * @param parentShell the parent of this control's shell
	 * @param statusFieldText the text to be used in the status field or {@code null} to hide the status field
	 */
	public Ttcn3HoverInfoControl(Shell parentShell, String statusFieldText) {
		super(parentShell, statusFieldText);
		create();
	}

	/**
	 * Called when hovering over the hover window 
	 * @param parentShell the parent of this control's shell
	 * @param statusFieldText the text to be used in the status field or {@code null} to hide the status field
	 * @param toolbar the manager or {@code null} if toolbar is not desired
	 */
	public Ttcn3HoverInfoControl(Shell parentShell, String statusFieldText, ToolBarManager toolbar) {
		super(parentShell, toolbar);
		this.toolbar = toolbar;
		create();
	}
	
	/**
	 * Called for code completion proposals
	 * @param parentShell the parent of this control's shell
	 */
	public Ttcn3HoverInfoControl(Shell parentShell) {
		super(parentShell, true);
		create();

		/** We force info view for completion proposals */ 
		actualType = HoverContentType.INFO;
		isProposalContent = true;
	}

	@Override
	@Deprecated
	public void setInformation(String information) {
		// setInput is used instead
	}

	@Override
	public boolean hasContents() {
		return content != null || markerContent != null;
	}

	@Override
	protected void createContent(Composite parent) {
		fParent = parent;
		setStatusText(STATUS_TEXT);
	}

	@Override
	public IInformationControlCreator getInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			@Override
            public IInformationControl createInformationControl(Shell parent) {
				ToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.VERTICAL | SWT.RIGHT);
            	return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString(), toolbar);
            }
		};
	}

	@Override
	public void setInput(final Object input) {
		setInput(input, false);
	}

	/**
	 * Same as {@link #setInput}, but shows the content independently from the {@link Ttcn3HoverContent} content
	 * @param input the object to be used as input for this control
	 * @param force whether to force to show the content of the {@link Ttcn3HoverContent}
	 */
	public void setInput(final Object input, final boolean force) {
		if (input instanceof Ttcn3HoverContent) {
			content = (Ttcn3HoverContent)input;
			handleTtcn3HoverContent(force);
		}
		if (input instanceof MarkerHoverContent) {
			markerContent = (MarkerHoverContent)input;
			handleMarkerHoverContent();
		}
		if (input instanceof String) {
			content = new Ttcn3HoverContent();
			content.addContent(HoverContentType.INFO, input.toString());
			handleTtcn3HoverContent(force);
		}
	}

	/**
	 * Handles the {@link Ttcn3HoverContent} input
	 * @param force whether to force to show the content of the {@link Ttcn3HoverContent}
	 */
	private void handleTtcn3HoverContent(final boolean force) {
		if (textViewer != null) {
			textViewer.setVisible(false);
		}
		if (browser == null) {
			try {
				browser = new Browser(fParent, SWT.NONE);
			} catch (SWTError e) {
				ErrorReporter.INTERNAL_ERROR("Could not instantiate hover window SWT Browser: " + e.getMessage());
				return;
			}
		}
		addFocusListener(focusListenerToStoreSize);
		if (force) {
			// If the hover window lost focus, it should be disposed
			addFocusListener(focusListenerToDispose);
		}

		// If force is enabled, get what is available from the Ttcn3HoverContent
		String text = null;
		if (force && content.hasText(HoverContentType.SOURCE)) {
			text = content.getText(HoverContentType.SOURCE);
		} else if (force && content.hasText(HoverContentType.INFO)) {
			text = content.getText(HoverContentType.INFO);
		} else {
			text = content.getText(actualType);
		}
		if (text != null) {
			browser.setText(text);
			browser.setVisible(true);
		};
		if (toolbar != null && !force) {
			final ToolBar tb = toolbar.createControl(fParent);
			tb.setCursor(Display.getCurrent().getSystemCursor(SWT.CURSOR_HAND));
			switch (ColorManager.getColorTheme()) {
			case Dark:
				tb.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_WHITE));
				break;
			case Light:
			default:
				tb.setForeground(getShell().getDisplay().getSystemColor(SWT.COLOR_LINK_FOREGROUND));
				break;
			}
			if (isProposalContent == false && content != null) {
				if (switchButton == null) {
					switchButton = new ToolItem(tb, SWT.FLAT | SWT.RIGHT);
					switchButton.addSelectionListener(switchListener);
				}
				switchButton.setText(getButtonText());
				switchButton.setImage(
						PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_SYNCED));
			}
		}
		setSizeConstraints(lastWidth, lastHeight);
		setSize(lastWidth, lastHeight);
	}

	/**
	 * Handles the {@link MarkerHoverContent} input
	 */
	private void handleMarkerHoverContent() {
		if (browser != null) {
			browser.setVisible(false);
		}
		if (textViewer == null) {
			textViewer = new StyledText(fParent, SWT.NONE);
			textViewer.setEditable(false);
			textViewer.setBackground(getShell().getBackground());
			textViewer.setFont(JFaceResources.getDialogFont());
		}

		final String markersText = markerContent.getText();
		if (markersText != null) {
			textViewer.setText(markersText);
			textViewer.setVisible(true);
		}
		if (toolbar != null) {
			final int nrOfProposals = markerContent.nrOfProposals();
			final ToolBar tb = toolbar.createControl(fParent);
			for (int i = 0; i < nrOfProposals; i++) {
				final HoverProposal prop = markerContent.getProposal(i);
				ToolItem button = new ToolItem(tb, SWT.FLAT | SWT.RIGHT);
				button.setText(prop.getLabel());
				final Image image = prop.getImage();
				if (image != null) {
					button.setImage(image);
				}
				button.addSelectionListener(new SelectionListener() {						
					@Override
					public void widgetSelected(SelectionEvent e) {
						prop.run(null);
						dispose();
					}
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						// Do nothing
					}
				});
				toolbarHeight += button.getBounds().height;
				toolbarWidth = toolbarWidth < button.getBounds().width ? button.getBounds().width : toolbarWidth;
			}
		}
		setSizeConstraints(SWT.DEFAULT, SWT.DEFAULT);
		setSize(computeSizeHint());
	}

	@Override
	public void setSize(final int width, final int height) {
		getShell().layout();
		if (toolbar != null && markerContent != null && markerContent.nrOfProposals() > 0) {
			getShell().setSize(Math.max(width, toolbarWidth), height + toolbarHeight - scrollBarSize);
		} else {
			getShell().setSize(width, height);
		}
	}

	public void setSize(final Point size) {
		setSize(size.x, size.y);
	}

	@Override
	public Point computeSizeConstraints(final int widthInChars, final int heightInChars) {
		Point point = super.computeSizeConstraints(widthInChars, heightInChars);
		point.x = Math.max(point.x, lastWidth);
		point.y = Math.max(point.y, lastHeight);
		return point;
	}

	private String getButtonText() {
		return MessageFormat.format(SWITCHBUTTON_TEXT_PATTERN, 
				actualType == HoverContentType.INFO ? HoverContentType.SOURCE : HoverContentType.INFO); 	
	}
}
