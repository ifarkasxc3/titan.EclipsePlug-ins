/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.types.EnumItem;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Collects proposals for an assignment statement
 * 
 * @author Miklos Magyari
 *
 */
public class AssignmentContext extends ProposalContext {
	private Reference reference;
	private Type_type typeType;
	private String name;
	
	public AssignmentContext(Module module, IFile file, Scope scope, int offset, AssignmentMatch aMatch) {
		super(module, file, scope, offset);
		this.name = aMatch.name;
		this.reference = aMatch.ref;
		this.typeType = aMatch.ttype;
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final CompilationTimeStamp timestamp = module.getLastCompilationTimeStamp();
	
		if (reference == null && typeType == null) {
			return;
		}
		
		if (scope == null) {
			return;
		}
		IType type = null;
		Type_type ttype;
		if (typeType != null) {
			ttype = typeType;
		} else {
			Assignment assignment = scope.getAssBySRef(timestamp, reference);
			if (assignment == null) {
				return;
			}
			type = assignment.getType(timestamp); 
			if (type == null) {
				return;
			}
			ttype = type.getTypetypeTtcn3();
		}
		switch (ttype) {
		case TYPE_TTCN3_ENUMERATED:
			TTCN3_Enumerated_Type etype = (TTCN3_Enumerated_Type)type;
			for (EnumItem item : etype.getEnumItems()) {
				propCollector.addProposal(item.getId(), ImageCache.getImageByType(ttype), "");
			}
			break;
		case TYPE_BITSTRING:
		case TYPE_CHARSTRING:
		case TYPE_HEXSTRING:
		case TYPE_INTEGER:
		case TYPE_OCTETSTRING:
		case TYPE_REAL:
		case TYPE_UCHARSTRING:
			List<Definition> defList = getAvailableDefsByType(ttype);
			for (Definition a : defList) {
				if (! a.getIdentifier().getName().equals(name)) {
					Ttcn3HoverContent doc = a.getHoverContent(null);
					propCollector.addProposal(a.getIdentifier(), ImageCache.getImageByType(ttype), 
						doc != null ? doc.getText(HoverContentType.INFO) : "");
				}
			}
			List<Assignment> aList = getAvailableAssignmentsByType(ttype);
			for (Assignment a : aList) {
				switch (a.getAssignmentType()) {
				case A_TYPE:
					break;
				default:
					Ttcn3HoverContent doc = null;
					if (a instanceof Definition) {						
						final Definition d = (Definition)a;
						doc = d.getHoverContent(null);
					}
					propCollector.addProposal(a.getIdentifier(), ImageCache.getImageByType(ttype),
						doc != null ? doc.getText(HoverContentType.INFO) : "");
				}
			}
			break;
		default:
		}
	}
}
