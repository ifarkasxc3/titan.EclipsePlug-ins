/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;
import org.eclipse.ui.IEditorPart;

/**
 * This class tries to find out the context for the current cursor position
 * It can be used to provide more suitable content assist proposals
 * 
 * @author Miklos Magyari
 *
 */
public class CompletionFinder {	
	private static final String IDENTIFIER = "([A-Za-z][A-Za-z0-9_]*)";
	private static final String ASSIGNOP = ":="; 
	private static final String DEFINITION = "[\\s]+" + IDENTIFIER + "[\\s]+" + IDENTIFIER + "[\\s]*" + ASSIGNOP;
	private static final String ASSIGNMENT = "(var|const)" + DEFINITION;
	private static final String FRIEND_REGEX = "[\\s]*friend module[\\s]*(.*)";
	
	public enum Completion { Component, Import, Friend, Assignment }
	public enum AssignmentType { Vardef, Varref, Constdef }
	
	private static Map<String,Type_type> basicTypes = new HashMap<String,Type_type>();
	private static Map<String,Completion> comps = new HashMap<String,Completion>();
	
	private IEditorPart editor;
	private Module module;
	private Scope scope;
	private IDocument document;
	private IFile file;
	private int offset;
	
	/** collection of context tokens */
	private List<String> tokenList = new ArrayList<String>();
	
	/** collection of whitespace strings separating context tokens */
	private List<String> wsList = new ArrayList<String>();

	static {
		basicTypes.put("bitstring", Type_type.TYPE_BITSTRING);
		basicTypes.put("charstring", Type_type.TYPE_CHARSTRING);
		basicTypes.put("float", Type_type.TYPE_REAL);
		basicTypes.put("hexstring", Type_type.TYPE_HEXSTRING);
		basicTypes.put("integer", Type_type.TYPE_INTEGER);
		basicTypes.put("octetstring", Type_type.TYPE_OCTETSTRING);

		comps.put("runs on", Completion.Component);
		comps.put("system", Completion.Component);
		comps.put("mtc", Completion.Component);
		comps.put("import from", Completion.Import);
		comps.put("friend module", Completion.Friend);
	}

	public CompletionFinder(Module module) {
		this.module = module;
	}

	public ProposalContext findOnLeft(final IEditorPart editor, int offset, final IDocument document, final IFile file) {
		scope = module.getSmallestEnclosingScope(offset);
		if (scope == null) {
			scope = module.getParentScope();
		}
		this.editor = editor;
		this.document = document;
		this.file = file;
		this.offset = offset;
		int ofs = offset - 1;
		if (-1 == ofs) {
			return null;
		}

		final StringBuilder left = new StringBuilder();

		try {
			char c;
			
			String ws = getWsStringAtOffset(ofs);
			if (ws.length() > 0) {
				wsList.add(ws);
				ofs -= ws.length();
			}
			
			final StringBuilder token = new StringBuilder();
			while (ofs >= 0) {
				c = document.getChar(ofs);
				if (Character.isAlphabetic(c) || Character.isDigit(c) || c == ',' || c == ':' || c == '=') {
					token.insert(0, c);
					ofs--;
					if (-1 == ofs) {
						return getContext(left.toString());
					}
				} else if (isWhiteSpace(c)) {
					ws = getWsStringAtOffset(ofs);
					if (ws.length() > 0) {
						wsList.add(ws);
						ofs -= ws.length();
						
						left.insert(0, token.toString());
						left.insert(0, " ");
						token.setLength(0);
					}
				} else {
					break;
				}
			}
		} catch (BadLocationException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}

		return getContext(left.toString().trim());
	}

	private String getWsStringAtOffset(int offset) {
		final StringBuilder wslist = new StringBuilder();
		
		for (;;) {
			char c;
			try {
				c = document.getChar(offset);
			} catch (BadLocationException e) {
				return wslist.toString();
			}
			if (isWhiteSpace(c)) {
				wslist.append(c);
				offset--;
				if (offset == -1) {
					return wslist.toString();
				}
			} else {
				break;
			}
		}
		return wslist.toString();
	}
	
	private ProposalContext getContext(String s) {
		if (s == null || s.isEmpty()) {
			return null;
		}
		
		final AssignmentMatch aMatch = matchAssignment(s);
		if (aMatch != null) {
			return new AssignmentContext(module, file, scope, offset, aMatch);
		} 
		
		List<String> friendModules = matchFriendModule(s);
		if (friendModules != null) {
			return new FriendContext(module, file, scope, offset, s.charAt(s.length() - 1), friendModules);
		}
		
		Completion c = comps.get(s);
		if (c == null) {
			return null;
		}
		switch (c) {
		case Import:
			return new ImportContext(module, file, scope, offset, null);
		default:
			return null;
		}
	}
	
	private List<String> matchFriendModule(String text) {
		int j = 0;
		for (int i = text.length()-1; i >= 0; i--, j++) {
			if (!isWhiteSpace(text.charAt(i))) {
				break;
			}
		}
		offset -= j;
		final Pattern p = Pattern.compile(FRIEND_REGEX);
		final Matcher m = p.matcher(text);
		if (m.matches()) {
			final List<String> mList = new ArrayList<String>();
			final String modules = m.group(1);
			if (modules.length() == 0) {
				return mList;
			}
			final String[] friendModules = modules.split("[,\\s]");
			for (int i = 0; i < friendModules.length; i++) {
				mList.add(friendModules[i].trim());
			}
			return mList;
		}
		return null;
	}

	private static boolean isWhiteSpace(char c) {
		return c == ' ' || c == '\t' || c == '\n' || c == '\r';
	}

	/** 
	 * Checks if the context is an assignment
	 * 
	 * @param text
	 * @return 
	 */
	private AssignmentMatch matchAssignment(String text) {
		final Pattern p = Pattern.compile(ASSIGNMENT);
		final Matcher m = p.matcher(text);
		AssignmentMatch aMatch = new AssignmentMatch();
		
		if (m.matches()) {
			final String vartype = m.group(2);
			final Type_type ttype = basicTypes.get(vartype);
			if (ttype != null) {
				aMatch.ttype = ttype;
			} else {				
				aMatch.ref = TTCN3ReferenceAnalyzer.parseForCompletion(file, vartype);
			}
			aMatch.name = m.group(3);
			return aMatch;
		}
		return null;
	}

	public List<String> getTokenList() {
		return tokenList;
	}

	public List<String> getWsList() {
		return wsList;
	}
}
