/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Module.module_type;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FriendModule;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for "friend module" statement
 * 
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public class FriendContext extends ProposalContext {
	private List<String> onTheFlyModules;
	private char endsWith;

	/**
	 * 
	 * @param module
	 * @param file
	 * @param scope
	 * @param modules list of modules that are already listed after 'friend module ...'
	 * @param needsComma whether we need to also insert a leading comma
	 */
	public FriendContext(Module module, IFile file, Scope scope, int offset, char endsWith, List<String> modules) {
		super(module, file, scope, offset);
		this.onTheFlyModules = modules;
		this.doFallback = false;
		this.endsWith = endsWith;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = null;
		if (endsWith != ',' && onTheFlyModules.size() > 0) {
			prefix = onTheFlyModules.get(onTheFlyModules.size() - 1);
		}
		
		final Set<String> knownModuleNames = sourceParser.getKnownModuleNames();
		boolean needsComma = knownModuleNames.contains(prefix); 
		knownModuleNames.remove(module.getIdentifier().getName());

		final List<FriendModule> friendModules = ((TTCN3Module) module).getFriendModules();		
		for (final FriendModule friendModule : friendModules) {
			knownModuleNames.remove(friendModule.getIdentifier().getName());
		}
		knownModuleNames.removeAll(onTheFlyModules);

		for (final String knownModuleName : knownModuleNames) {
			final Identifier tempIdentifier = new Identifier(Identifier_type.ID_NAME, knownModuleName);
			final Module tempModule = sourceParser.getModuleByName(knownModuleName);
			if (tempModule.getModuletype() != module_type.TTCN3_MODULE) {
				continue;
			}
				
			String replacementString;
			if (onTheFlyModules.size() == 0 && endsWith == 'e') {
				replacementString = " " + tempIdentifier.getName();
			} else {
				replacementString = tempIdentifier.getName();
			}
			final String proposalString = tempIdentifier.getName();
			int relevance = 0;
			int offset = this.offset;
			int cursorPos;
			int replacementLen = 0;
			if (needsComma) {
				replacementString = ", " + replacementString;
				cursorPos = replacementString.length();
			} else if (prefix == null) { 
				cursorPos = replacementString.length();
			} else {
				offset = offset - prefix.length();
				replacementLen = prefix.length();
				cursorPos = prefix.length() + replacementString.length() - replacementLen;
				if (knownModuleName.startsWith(prefix)) {
					relevance = 1;
				}
			} 
			
			final CompletionProposal proposal = new CompletionProposal(replacementString, offset, replacementLen, 
					cursorPos, ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(proposalString), null, tempModule.getHoverContent(null), relevance);
			propCollector.addProposal(proposal);
		}
		propCollector.sortAll();
	}
}
