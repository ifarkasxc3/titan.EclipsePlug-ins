/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for "import from" statement
 * 
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public class ImportContext extends ProposalContext {
	public ImportContext(Module module, IFile file, Scope scope, int offset, String prefix) {
		super(module, file, scope, offset, prefix);
		this.doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final Set<String> knownModuleNames = sourceParser.getKnownModuleNames();
		knownModuleNames.remove(module.getIdentifier().getName());

		final List<Module> importedModules = module.getImportedModules();
		for (final Module importedModule : importedModules) {
			knownModuleNames.remove(importedModule.getIdentifier().getName());
		}

		for (final String knownModuleName : knownModuleNames) {
			final Module tempModule = sourceParser.getModuleByName(knownModuleName);
			final int replacementLength = prefix == null ? 0 : prefix.length();
			final CompletionProposal proposal = new CompletionProposal(knownModuleName, offset, replacementLength, 
					knownModuleName.length(), ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(knownModuleName), null, tempModule.getHoverContent(null), 0);
			propCollector.addProposal(proposal);

			final String replacementString = knownModuleName + " all";
			final CompletionProposal proposal2 = new CompletionProposal(replacementString, offset, replacementLength, 
					replacementString.length(), ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(replacementString), null, tempModule.getHoverContent(null), 0);
			propCollector.addProposal(proposal2);
		}
		propCollector.sortAll();
	}
}
