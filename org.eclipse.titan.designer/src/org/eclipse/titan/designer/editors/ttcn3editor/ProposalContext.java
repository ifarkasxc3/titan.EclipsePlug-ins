/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignments;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;

/**
 * Base class for different proposal contexts
 * 
 * @author Miklos Magyari
 *
 */
public abstract class ProposalContext {
	protected ProjectSourceParser sourceParser;
	protected Module module;
	protected Scope scope;
	protected int offset;
	protected String prefix = null;
	protected boolean doFallback;
	private CompilationTimeStamp timestamp;
	
	public ProposalContext(Module module, IFile file, Scope scope, int offset) {
		sourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		this.module = module;
		this.scope = scope;
		this.offset = offset;
		timestamp = module.getLastCompilationTimeStamp();
		doFallback = true;
	}

	public ProposalContext(Module module, IFile file, Scope scope, int offset, String prefix) {
		this(module, file, scope, offset);
		this.prefix = prefix;
	}
	/**
	 * Adds context-specific proposals to the given collector
	 * 
	 * @param propCollector
	 */
	public abstract void getProposals(ProposalCollector propCollector);
	
	/**
	 * Collects the list of available definitions in the current scope
	 * 
	 * @param typeType
	 * @return
	 */
	protected List<Definition> getAvailableDefsByType(Type_type typeType) {
		List<Definition> refs = new ArrayList<Definition>();
			
		// FIXME : timestamp should not be null; this check is to avoid stack overflow
		if (timestamp == null) {
			return refs;
		}

		StatementBlock sb = null;
		if (scope instanceof StatementBlock ) {
			sb = (StatementBlock)scope; 
		} else if (scope != null) {
			sb = scope.getStatementBlockScope().getMyStatementBlock();
		} else {
			return refs;
		}
		
		if (sb == null) {
			return refs; 
		}

		final Map<String, Definition> map = sb.getDefinitionMap();
		if (map == null) {
			return refs;
		}
		for (Map.Entry<String, Definition> e : map.entrySet()) {
			final Definition def = e.getValue();
			if (def.getType(timestamp).getTypetype() == typeType) {
				refs.add(def);
			}
		}
		
		return refs;
	}
	
	protected List<Assignment> getAvailableAssignmentsByType(Type_type typeType) {
		List<Assignment> refs = new ArrayList<Assignment>();
		
		// FIXME : timestamp should not be null; this check is to avoid stack overflow
		if (timestamp == null) {
			return refs;
		}
		
		StatementBlock sb = null;
		if (scope instanceof StatementBlock ) {
			sb = (StatementBlock)scope; 
		} else if (scope != null) {
			sb = scope.getStatementBlockScope().getMyStatementBlock();
		} else {
			return refs;
		}
		
		if (sb == null) {
			return refs; 
		}
		
		final Assignments asses = scope.getAssignmentsScope();
		final int nrAss = asses.getNofAssignments();
		for (int i = 0; i < nrAss; i++) {
			Assignment ass = asses.getAssignmentByIndex(i);
			IType aType = ass.getType(timestamp);
			if (aType != null) {
				if (ass.getType(timestamp).getTypetype() == typeType) {
					refs.add(ass);
				}
			}
		}
		return refs;
	}

	/**
	 * Returns whether fallback is needed to the old proposal list compilation
	 * @return Whether fallback is needed to the old proposal list compilation
	 */
	public boolean doFallback() {
		return doFallback;
	}
}
