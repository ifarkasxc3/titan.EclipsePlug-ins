/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ControlPart;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Group;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.designer.editors.BaseTextHover;
import org.eclipse.titan.designer.editors.IReferenceParser;
import org.eclipse.titan.designer.editors.controls.HoverProposal;
import org.eclipse.titan.designer.editors.controls.MarkerHoverContent;
import org.eclipse.titan.designer.editors.controls.PeekSource;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.MarkerAnnotation;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class TextHover extends BaseTextHover implements ITextHoverExtension, ITextHoverExtension2 {
	private final ISourceViewer sourceViewer;
	private final IEditorPart editor;

	public TextHover(final ISourceViewer sourceViewer, final IEditorPart editor) {
		this.sourceViewer = sourceViewer;
		this.editor = editor;
	}

	@Override
	protected ISourceViewer getSourceViewer() {
		return sourceViewer;
	}

	@Override
	protected IEditorPart getTargetEditor() {
		return editor;
	}

	@Override
	protected IReferenceParser getReferenceParser() {
		return new TTCN3ReferenceParser(false);
	}

	/**
	 * Ttcn3 editor hover support
	 *  
	 * @param textViewer
	 * @param hoverRegion
	 * @return
	 */
	@Override
	public Object getHoverInfo2(final ITextViewer textViewer, final IRegion hoverRegion) {
		IDocument doc = textViewer.getDocument();
		final IFile file = editor.getEditorInput().getAdapter(IFile.class);

		final String id = getIdentifier(doc, hoverRegion.getOffset());
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		Scope scope = null;

		if (tempModule != null) {
			scope = tempModule.getSmallestEnclosingScope(hoverRegion.getOffset());
			if (scope == null) {
				scope = tempModule;
			}
		} else {
			return "";
		}

		final IAnnotationModel annotationModel = sourceViewer.getAnnotationModel();
		if (annotationModel != null) {
			final Iterator<?> iterator = annotationModel.getAnnotationIterator();
			StringBuilder markerInfo = new StringBuilder();
			List<HoverProposal> proposals = new ArrayList<HoverProposal>(); 
			while (iterator.hasNext()) {
				final Object o = iterator.next();
				if (o instanceof MarkerAnnotation) {
					final MarkerAnnotation actualMarker = (MarkerAnnotation) o;
					final Position markerPosition = annotationModel.getPosition(actualMarker);
					if (markerPosition.getOffset() <= hoverRegion.getOffset()
							&& markerPosition.getOffset() + markerPosition.getLength() >= hoverRegion.getOffset()) {
						String message = actualMarker.getText();
						if (message == null) {
							ErrorReporter.INTERNAL_ERROR("The marker at " + markerPosition.getOffset() + " does not seem to have any text");
						}
						if (markerInfo == null) {
							// FIXME error handling
						}
						markerInfo.append(message);
						Object attr = null;
						try {
							attr = actualMarker.getMarker().getAttribute(HoverProposal.PROPOSAL);
						} catch (CoreException e) {
							// TODO Auto-generated catch block
						}
						if (attr instanceof HoverProposal[]) {
							HoverProposal[] propsInMarker = (HoverProposal[])attr;
							for (int i = 0; i < propsInMarker.length; i++) {
								HoverProposal propEntry = propsInMarker[i];
								propEntry.setMarker(actualMarker.getMarker(), actualMarker);
								proposals.add(propEntry);
							}
						}
					}
				}
				if (markerInfo.length() > 0) {
					HoverProposal[] propArr = {};
					propArr = proposals.toArray(propArr);
					return new MarkerHoverContent(markerInfo.toString(), propArr);
				}
			}
		}

		Ttcn3HoverContent info = null;
		// Get documentation comments for other elements that are not covered by assignments/definitions:
		// 1. control part and locale module definition 
		if (scope instanceof Module || scope instanceof ControlPart) {
			info = scope.getHoverContent(editor);
			return info;
		}
		// 2. groups; by the way, selective importation is not yet supported, so groups are meaningless currently
		if (scope instanceof Definitions) {
			final Group group = ((Definitions) scope).getGroupByName(id);
			if (group != null) {
				info = group.getHoverContent(editor);
				return info;
			}
		}
		// 3. imported modules
		List<Module> importedModules = scope.getModuleScope().getImportedModules();
		for (int i = 0; i < importedModules.size(); i++) {
			final Module module = importedModules.get(i);
			if (module.getName().equals(id)) {
				info = module.getHoverContent(editor);
				return info;
			}
		}
		// 4. assignments/definitions
		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(hoverRegion.getOffset());
		scope.getModuleScope().accept(visitor);

		final Declaration declaration = visitor.getReferencedDeclaration();
		if (declaration == null) {
			return "";
		}

		Assignment assignment = declaration.getAssignment();
		if (assignment instanceof Definition) {
			info = ((Definition) assignment).getHoverContent(editor);
			return info;
		}

		return null;
	}

	/**
	 * Legacy interface method implemented for completeness
	 * 
	 * getHoverInfo2 is used instead
	 */
	@Deprecated
	@Override
	public String getHoverInfo(final ITextViewer textViewer, final IRegion hoverRegion) {
		return null;
	}

	private String getIdentifier(IDocument doc, int pos) {
		try {
			char c = doc.getChar(pos);
			if (isIdChar(c) == false) {
				return "";
			}

			int start = 0;
			for (int pt = pos; pt >= 0 && isIdChar(doc.getChar(pt)) ; pt--) {
				start = pt;
			}

			int end = 0;
			for (int pt = pos; pt < doc.getLength() && isIdChar(doc.getChar(pt)) ; pt++) {
				end = pt;
			}
			String s = doc.get(start, end - start + 1);
			return s;
		} catch (Exception e) {

		}
		return null;
	}

	private boolean isIdChar(char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_';
	}

	@Override
	public IInformationControlCreator getHoverControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(Shell parent) {
				return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString());
			}
		};
	}
}
