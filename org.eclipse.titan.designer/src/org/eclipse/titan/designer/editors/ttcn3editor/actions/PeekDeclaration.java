/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.designer.editors.controls.PeekSource;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Editor;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

/**
 * Action code for peek declaration feature
 * 
 * @author Miklos Magyari
 *
 */
public class PeekDeclaration extends AbstractHandler implements IEditorActionDelegate {
	@Override
	public void run(IAction action) {
		peekDefinition();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// Do nothing
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		peekDefinition();
		return null;
	}

	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// Do nothing
	}

	private void peekDefinition() {
		IEditorPart targetEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		ISelection selection = TextSelection.emptySelection();

		if (targetEditor == null || !(targetEditor instanceof TTCN3Editor)) {
			return;
		}

		final IFile file = (IFile) targetEditor.getEditorInput().getAdapter(IFile.class);

		int offset;
		if (selection instanceof TextSelection && !selection.isEmpty() && !"".equals(((TextSelection) selection).getText())) {
			final TextSelection tSelection = (TextSelection) selection;
			offset = tSelection.getOffset() + tSelection.getLength();
		} else {
			offset = ((TTCN3Editor) targetEditor).getCarretOffset();
		}

		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module module = projectSourceParser.containedModule(file);

		if (module == null) {
			return;
		}

		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(offset);
		module.accept(visitor);
		final Declaration decl = visitor.getReferencedDeclaration();
		Shell shell = targetEditor.getSite().getShell();

		Ttcn3HoverInfoControl con = new Ttcn3HoverInfoControl(shell, null);
		Control control = targetEditor.getAdapter(Control.class);

		if (control instanceof StyledText) {
			final StyledText text = (StyledText)control;
			Point caretLocation = text.getLocationAtOffset(text.getCaretOffset());
			Point displayLocation = text.toDisplay(caretLocation);
			final Font textFont= JFaceResources.getTextFont();
			final FontData fontData[] = textFont.getFontData();
			displayLocation.y += (fontData[0].getHeight() * 2);
			con.setLocation(displayLocation);
		}

		final Ttcn3HoverContent content = new Ttcn3HoverContent();
		PeekSource.addStyledSource(PeekSource.getPeekSource(targetEditor, decl.getLocation()), content);
		con.setInput(content, true);
		con.setVisible(true);
		con.setFocus();
	}
}
