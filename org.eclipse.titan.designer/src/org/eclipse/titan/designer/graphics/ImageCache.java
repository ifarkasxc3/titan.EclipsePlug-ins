/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.graphics;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.TTCN3.types.BitString_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.CharString_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Float_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.HexString_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Integer_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.OctetString_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Generic class to cache images inside the Designer plug-in.
 * 
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class ImageCache {
	private static final String ICONS_SUBDIR = "icons/";

	private static Map<ImageDescriptor, Image> imageCache = new ConcurrentHashMap<ImageDescriptor, Image>();
	private static Map<Type_type, String> typeCache = new HashMap<Type_type, String>();

	private ImageCache() {
		// Hide constructor
	}
	
	static {
		typeCache.put(Type_type.TYPE_BITSTRING, (new BitString_Type().getOutlineIcon()));
		typeCache.put(Type_type.TYPE_CHARSTRING, (new CharString_Type().getOutlineIcon()));
		typeCache.put(Type_type.TYPE_TTCN3_ENUMERATED, (new TTCN3_Enumerated_Type(null).getOutlineIcon()));
		typeCache.put(Type_type.TYPE_HEXSTRING, (new HexString_Type().getOutlineIcon()));
		typeCache.put(Type_type.TYPE_INTEGER, (new Integer_Type().getOutlineIcon()));
		typeCache.put(Type_type.TYPE_OCTETSTRING, (new OctetString_Type().getOutlineIcon()));
		typeCache.put(Type_type.TYPE_REAL, (new Float_Type().getOutlineIcon()));
	}

	/**
	 * Creates an ImageDescriptor from the image's name, whose root is the
	 * icons directory.
	 * 
	 * @param name
	 *                the name of the image starting from the icons
	 *                directory
	 * @return the created ImageDescriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String name) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(ProductConstants.PRODUCT_ID_DESIGNER, ICONS_SUBDIR + name);
	}

	/**
	 * Creates and returns the image identified by the provided name. This
	 * always gives back the same image instance for the same file name
	 * 
	 * @param name
	 *                the name of the image starting from the icons
	 *                directory
	 * @return the created ImageDescriptor
	 */
	public static Image getImage(final String name) {
		final ImageDescriptor descriptor = getImageDescriptor(name);
		if (descriptor == null) {
			return null;
		}
		if (imageCache.containsKey(descriptor)) {
			return imageCache.get(descriptor);
		}

		final Image image = descriptor.createImage();
		imageCache.put(descriptor, image);
		return image;
	}
	
	/**
	 * Gets the icon image based on Type_type
	 *  
	 * @param tt
	 * @return
	 */
	public static Image getImageByType(Type_type tt) {
		return typeCache.containsKey(tt) ? getImage(typeCache.get(tt)) : null;
	}
}
