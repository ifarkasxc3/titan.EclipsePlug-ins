parser grammar Ttcn3DocCommentParser;

/*
 ******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Parser grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 * author Adam Knapp
 */

options {
    tokenVocab=Ttcn3DocCommentLexer;
}

@header
{
    import java.lang.StringBuilder;
    import org.eclipse.titan.designer.AST.*;
}

pr_DocComment[DocumentComment documentComment]:
(   pr_BlockComment[documentComment]
|   pr_LineComments[documentComment]
)+;

pr_BlockComment[DocumentComment documentComment]:
    BLOCK_BEGIN 
    (   pr_Tag[documentComment]
    |   WS
    |   NEWLINE
    |   IDENTIFIER
    |   FREETEXT
    )*
    BLOCK_END
;

pr_LineComments[DocumentComment documentComment]:
    (   LINE
        ( pr_Tag[documentComment] )?
        NEWLINE?
    )+
;

pr_AuthorTag[DocumentComment documentComment]:
    AUTHOR
    txt = pr_FreeText { documentComment.addAuthor( $txt.text ); }
;

pr_ConfigTag[DocumentComment documentComment]:
    CONFIG
    txt = pr_FreeText { documentComment.addConfig( $txt.text ); }
;

pr_DescTag[DocumentComment documentComment]:
    DESC
    txt = pr_FreeText { documentComment.addDesc( $txt.text ); }
;

pr_ExceptionTag[DocumentComment documentComment]
@init {
    String freetext = null;
}:
    EXCEPTION
    id = pr_Identifier
    WS?
    ( ft = pr_FreeText { freetext = $ft.text; } )?
{
    documentComment.addException( $id.text, freetext );
};

pr_MemberTag[DocumentComment documentComment]
@init {
    String freetext = null;
}:
    MEMBER
    id = pr_Identifier
    WS?
    ( ft = pr_FreeText { freetext = $ft.text; } )?
{
    documentComment.addMember( $id.text, freetext );
};

pr_ParamTag[DocumentComment documentComment]
@init {
    String freetext = null;
}:
    PARAM
    id = pr_Identifier
    WS?
    ( ft = pr_FreeText { freetext = $ft.text; } )?
{
    documentComment.addParam( $id.text, freetext );
};

pr_PriorityTag[DocumentComment documentComment]:
    PRIORITY
    id = pr_Identifier { documentComment.addPriority( $id.text ); }
;

pr_PurposeTag[DocumentComment documentComment]:
    PURPOSE
    txt = pr_FreeText { documentComment.addPurpose( $txt.text ); }
;

pr_ReferenceTag[DocumentComment documentComment]:
    REFERENCE
    txt = pr_FreeText { documentComment.addReference( $txt.text ); }
;

pr_RemarkTag[DocumentComment documentComment]:
    REMARK
    txt = pr_FreeText { documentComment.addRemark( $txt.text ); }
;

pr_RequirementTag[DocumentComment documentComment]:
    REQUIREMENT
    txt = pr_FreeText { documentComment.addRequirement( $txt.text ); }
;

pr_ReturnTag[DocumentComment documentComment]:
    RETURN
    txt = pr_FreeText { documentComment.addReturn( $txt.text ); }
;

pr_SeeTag[DocumentComment documentComment]:
    SEE
    id = pr_Identifier { documentComment.addSee( $id.text ); }
;

pr_SinceTag[DocumentComment documentComment]:
    SINCE
    txt = pr_FreeText { documentComment.addSince( $txt.text ); }
;

pr_StatusTag[DocumentComment documentComment]:
    STATUS
    txt = pr_FreeText { documentComment.addStatus( $txt.text ); }
;

pr_UrlTag[DocumentComment documentComment]:
    URL
    txt = pr_FreeText { documentComment.addUrl( $txt.text ); }
;

pr_VerdictTag[DocumentComment documentComment]
@init {
    String freetext = null;
}:
    VERDICT
    id = pr_Identifier
    WS?
    ( ft = pr_FreeText { freetext = $ft.text; } )?
{
    documentComment.addVerdict( $id.text, freetext );
};

pr_VersionTag[DocumentComment documentComment]:
    VERSION
    txt = pr_FreeText { documentComment.addVersion( $txt.text ); }
;

pr_Tag[DocumentComment documentComment]:
(   pr_AuthorTag[documentComment]
|   pr_ConfigTag[documentComment]
|   pr_DescTag[documentComment]
|   pr_ExceptionTag[documentComment]
|   pr_MemberTag[documentComment]
|   pr_ParamTag[documentComment]
|   pr_PriorityTag[documentComment]
|   pr_PurposeTag[documentComment]
|   pr_ReferenceTag[documentComment]
|   pr_RemarkTag[documentComment]
|   pr_RequirementTag[documentComment]
|   pr_ReturnTag[documentComment]
|   pr_SeeTag[documentComment]
|   pr_SinceTag[documentComment]
|   pr_StatusTag[documentComment]
|   pr_UrlTag[documentComment]
|   pr_VerdictTag[documentComment]
|   pr_VersionTag[documentComment]
);

pr_Identifier returns[String text]:
    IDENTIFIER { $text = $IDENTIFIER.getText(); }
;

pr_FreeText returns[String text]
@init {
    StringBuilder sb = new StringBuilder();
}:
    ( 
        WS?
        (   IDENTIFIER { sb.append( $IDENTIFIER.getText()); } 
        |   FREETEXT { sb.append( $FREETEXT.getText()); } 
        )
        ( WS { sb.append( $WS.getText() ); } )?
        ( NEWLINE { sb.append('\n'); })?
        ( WS { sb.append(' '); } )?
        LINE?
    )+
{
    $text = sb.toString();
}
;