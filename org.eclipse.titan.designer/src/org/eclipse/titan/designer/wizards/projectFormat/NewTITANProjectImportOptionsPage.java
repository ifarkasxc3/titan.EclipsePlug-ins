/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.wizards.projectFormat;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * @author Kristof Szabados
 * */
class NewTITANProjectImportOptionsPage extends WizardPage {
	private static final String TITLE = "TITAN Project importation options";

	private Button limitImportProcesses;
	private boolean islimitImportProcesses = false;
	private Button openPropertiesForAllImports;
	private boolean isOpenPropertiesForAllImports = false;
	private Button skipExistingProjects;
	private boolean isSkipExistingProjects = true;

	public NewTITANProjectImportOptionsPage() {
		super(TITLE);
	}

	@Override
	public String getDescription() {
		return "Finetune the amount of data saved about the project";
	}

	@Override
	public String getTitle() {
		return TITLE;
	}

	public boolean islimitImportProcesses() {
		return islimitImportProcesses;
	}

	public boolean isOpenPropertiesForAllImports() {
		return isOpenPropertiesForAllImports;
	}

	public boolean isSkipExistingProjects() {
		return isSkipExistingProjects;
	}

	@Override
	public void createControl(final Composite parent) {
		final Composite pageComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		pageComposite.setLayout(layout);
		final GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		pageComposite.setLayoutData(data);

		openPropertiesForAllImports = new Button(pageComposite, SWT.CHECK);
		openPropertiesForAllImports.setText("Open the preference page for all imported sub projects");
		openPropertiesForAllImports.setEnabled(true);
		openPropertiesForAllImports.setSelection(false);
		isOpenPropertiesForAllImports = false;
		openPropertiesForAllImports.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				isOpenPropertiesForAllImports = openPropertiesForAllImports.getSelection();
			}
		});

		skipExistingProjects = new Button(pageComposite, SWT.CHECK);
		skipExistingProjects.setText("Skip existing projects on import");
		skipExistingProjects.setEnabled(true);
		skipExistingProjects.setSelection(true);
		isSkipExistingProjects = true;
		skipExistingProjects.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				isSkipExistingProjects = skipExistingProjects.getSelection();
			}
		});

		limitImportProcesses = new Button(pageComposite, SWT.CHECK);
		limitImportProcesses.setText("Limit the number of parallel import processes");
		limitImportProcesses.setToolTipText("This might need to turn on when the OS runs out of threads, "
				+ "i.e. when 'OutOfMemoryError: unable to create native thread' exception is thrown");
		limitImportProcesses.setEnabled(true);
		limitImportProcesses.setSelection(false);
		islimitImportProcesses = false;
		limitImportProcesses.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				islimitImportProcesses = limitImportProcesses.getSelection();
			}
		});

		setControl(pageComposite);
	}
}
