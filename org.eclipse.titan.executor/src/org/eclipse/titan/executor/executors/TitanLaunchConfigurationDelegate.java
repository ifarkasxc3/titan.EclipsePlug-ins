/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.executor.executors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.debug.internal.ui.DebugUIPlugin;
import org.eclipse.debug.internal.ui.IInternalDebugUIConstants;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.executor.perspectives.ExecutingPerspective;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

/**
 * @author Szabolcs Beres
 * @author Jeno Balasko
 * */
@SuppressWarnings("restriction")
public abstract class TitanLaunchConfigurationDelegate extends LaunchConfigurationDelegate {
	private final String ALWAYS = "always";
	private final String PROMPT = "prompt";
	private final String MODE_RUN = "run";

	@Override
	public ILaunch getLaunch(final ILaunchConfiguration configuration, final String mode) throws CoreException {
		return new Launch(configuration, mode, null);
	}

	@Override
	protected IProject[] getBuildOrder(final ILaunchConfiguration configuration, final String mode) throws CoreException {
		final IResource[] resources = configuration.getMappedResources();
		if( resources == null) { return null; }
		final List<IProject> result = new ArrayList<IProject>();
		for (final IResource resource : resources) {
			if (resource instanceof IProject) {
				result.add((IProject) resource);
			}
		}

		return computeReferencedBuildOrder(result.toArray(new IProject[result.size()]));
	}

	@Override
	protected IProject[] getProjectsForProblemSearch(final ILaunchConfiguration configuration, final String mode) throws CoreException {
		return getBuildOrder(configuration, mode);
	}

	/**
	 * Shows the Titan Executing Perspective to the user if the running is not
	 * in headless mode
	 */
	protected void showExecutionPerspective(ILaunchConfigurationType configurationType) {
		if (!PlatformUI.isWorkbenchRunning()) {
			return; // headless mode
		}

		final IWorkbench workbench = PlatformUI.getWorkbench();
		final DebugUIPlugin debugPlugin = DebugUIPlugin.getDefault();
		String pref = debugPlugin.getPreferenceStore().getString(IInternalDebugUIConstants.PREF_SWITCH_TO_PERSPECTIVE);
		if (pref == null || pref.isEmpty()) {
			pref = ALWAYS;
		}
		String perspectiveId = debugPlugin.getPerspectiveManager().getLaunchPerspective(configurationType, MODE_RUN);
		if (perspectiveId == null || pref.isEmpty()) {
			perspectiveId = ExecutingPerspective.class.getClass().getName();
		}

		try {
			if (pref.equals(ALWAYS)) {
				workbench.showPerspective(perspectiveId, workbench.getActiveWorkbenchWindow());
			} else if (pref.equals(PROMPT)) {
				boolean result = MessageDialog.openConfirm(new Shell(), "Confirm Perspective Switch", "Do you want to switch perspective?");
				if (result) {
					workbench.showPerspective(perspectiveId, workbench.getActiveWorkbenchWindow());
				}
			}
		} catch (WorkbenchException e) {
			ErrorDialog.openError(null, "Selecting the perspective failed: " + perspectiveId, e.getMessage(), e.getStatus());
		}
	}

}
